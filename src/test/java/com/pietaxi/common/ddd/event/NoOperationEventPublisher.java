package com.pietaxi.common.ddd.event;

/**
 * @author pawel szymczyk
 */
public class NoOperationEventPublisher implements EventPublisher {
    @Override
    public void publishEvent(DomainEvent domainEvent) {

    }
}
