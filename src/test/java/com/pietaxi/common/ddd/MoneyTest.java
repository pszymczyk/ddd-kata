package com.pietaxi.common.ddd;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.pietaxi.common.ddd.vo.Money;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

/**
 * @author pawel szymczyk
 */
@RunWith(JUnitParamsRunner.class)
public class MoneyTest {

    @Test
    @Parameters({
            "0.00, 0",
            "8.00, 8",
            "8.000000, 8",
            "8.000000, 8.00",
    })
    public void shouldIgnoreIrrelevantZeros(String x, String y) {
        //expect
        assertThat(Money.of(x).equals(Money.of(y))).isTrue();
    }

    @Test
    @Parameters({
            "0.129, 0.13",
            "0.125, 0.13",
            "0.124, 0.12",
            "0.1249, 0.12",
            "0.9999, 1"
    })
    public void shouldRoundToExpectedScale(String x, String y) {
        //expect
        assertThat(Money.of(x)).isEqualTo(Money.of(y));
    }

    @Test
    @Parameters({
            "2, 2, 4",
            "2, 1, 2",
            "2, 0, 0",
            "9.9999, 2, 20",
    })
    public void shouldMultiplyMoneyByInt(String money, int multiplier, String result) {
        //expect
        assertThat(Money.of(money).times(multiplier)).isEqualTo(Money.of(result));
    }

    @Test
    @Parameters({
            "2, 17179869256, 34359738512"
    })
    public void shouldMultiplyMoneyByLong(String money, long multiplier, String result) {
        //expect
        assertThat(Money.of(money).times(multiplier)).isEqualTo(Money.of(result));
    }

    @Test
    @Parameters({
            "2, 0, 2",
            "2, 3, 5"
    })
    public void shouldAddMoneyToMoney(String x, String y, String z) {
        //expect
        assertThat(Money.of(x).add(Money.of(y))).isEqualTo(Money.of(z));
    }

    @Test
    @Parameters(method = "incorrectMoneyFormat")
    public void shouldThrowExceptionWhenTryToCreateMoneyFromIncorrectInput(String x) {
        //expect
        assertThatExceptionOfType(NumberFormatException.class).isThrownBy(() -> Money.of(x));
    }

    private Object[] incorrectMoneyFormat() {
        return new Object[]{
                new Object[]{""},
                new Object[]{"one thousand"},
                new Object[]{"XXI"},
                new Object[]{"3.3.3"},
                new Object[]{"3,2"},
                new Object[]{"3 200 000"},
                new Object[]{"3_200_000"},
        };
    }

    @Test
    public void shouldThrowNullPointerExceptionWhenTryToCreateMoneyFromNull() {
        //expect
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> Money.of(null));
    }

    @Test
    public void shouldBeIndiscernible() {
        //when
        Set<Money> moneySet = new HashSet<>();
        moneySet.add(Money.of("100"));
        moneySet.add(Money.of("100"));
        moneySet.add(Money.of("100"));

        //expect
        assertThat(moneySet).hasSize(1);
    }
}