package com.pietaxi.rides;

import java.time.Clock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.pietaxi.common.CommonConfiguration;
import com.pietaxi.common.ddd.DomainTools;
import com.pietaxi.common.ddd.event.DomainEvent;
import com.pietaxi.common.ddd.event.EventPublisher;
import com.pietaxi.crm.CrmFacade;
import com.pietaxi.rides.application.RidesApplicationConfiguration;
import com.pietaxi.rides.domain.model.HistoricalRidesRepository;
import com.pietaxi.rides.domain.model.PersistentRideFixture;
import com.pietaxi.rides.domain.model.Ride;
import com.pietaxi.rides.domain.model.RidesRepository;
import com.pietaxi.rides.port.adapter.memory.RidesMemoryAdapterConfiguration;
import com.pietaxi.rides.port.adapter.rest.RidesRestAdapter.FinishRideRequest;
import com.pietaxi.rides.port.adapter.rest.RidesRestAdapterConfiguration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author pawel szymczyk
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {
                RidesApplicationConfiguration.class,
                RidesMemoryAdapterConfiguration.class,
                RidesRestAdapterConfiguration.class,
                RidesIntegrationTest.Config.class,
                CommonConfiguration.class,
        })
@EnableAutoConfiguration
public class RidesIntegrationTest {

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    PersistentRideFixture persistentRideFixture;

    @Autowired
    RidesRepository ridesRepository;

    @Autowired
    HistoricalRidesRepository historicalRidesRepository;

    @Autowired
    EventPublisher eventPublisher;

    @Test
    public void shouldFinishRide() {
        //given
        Ride ride = persistentRideFixture.dummyPresentRide();
        FinishRideRequest finishRideRequest = new FinishRideRequest();
        finishRideRequest.setRideId(ride.getId().asString());
        finishRideRequest.setLongitude(100L);
        finishRideRequest.setLatitude(100L);

        //when
        ResponseEntity<?> response = restTemplate.postForEntity("/finishedrides", finishRideRequest, Void.class);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(ridesRepository.findById(ride.getId()).isRemoved()).isTrue();
        assertThat(historicalRidesRepository.findById(ride.getId())).isNotNull();
        verify(eventPublisher).publishEvent(any(DomainEvent.class));
    }

    @Configuration
    static class Config {

        @Autowired
        RidesRepository ridesRepository;

        @Autowired
        Clock clock;

        @Bean
        @Primary
        EventPublisher eventPublisher() {
            return mock(EventPublisher.class);
        }

        @Bean
        PersistentRideFixture persistentRideFixture() {
            return new PersistentRideFixture(ridesRepository, new DomainTools(eventPublisher(), clock));
        }

        @Bean
        CrmFacade crmFacade() {
            return mock(CrmFacade.class);
        }
    }
}
