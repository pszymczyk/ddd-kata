package com.pietaxi.rides.domain.model;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.junit.Test;

import com.pietaxi.common.ddd.DomainTools;
import com.pietaxi.common.ddd.event.EventPublisher;
import com.pietaxi.crm.CrmFacade;
import com.pietaxi.sharedkernel.DriverId;
import com.pietaxi.sharedkernel.PassengerId;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author pawel szymczyk
 */
public class RideFactoryTest {

    @Test
    public void shouldCreateRide() {
        //given
        PassengerId passengerId = PassengerId.random();
        DriverId driverId = DriverId.random();
        Driver driver = new Driver(driverId);
        DriverRepository driverRepository = mock(DriverRepository.class);
        when(driverRepository.findById(driverId)).thenReturn(driver);
        CrmFacade crmFacade = mock(CrmFacade.class);
        Clock clock = Clock.fixed(Instant.parse("2007-12-03T10:15:30.00Z"), ZoneOffset.UTC);
        when(crmFacade.isUserActive(passengerId)).thenReturn(true);
        RideFactory rideFactory = new RideFactory(
                crmFacade,
                driverRepository,
                new DomainTools(
                        mock(EventPublisher.class),
                        clock
                )
        );

        //when
        Ride ride = rideFactory.create(passengerId, driverId, 1, 1);

        //then
        RideSnapshot rideSnapshot = ride.finish(new Location(2, 2));
        assertThat(rideSnapshot.getRideId()).isNotNull();
        assertThat(rideSnapshot.getDriverId()).isEqualTo(driverId);
        assertThat(rideSnapshot.getPassengerId()).isEqualTo(passengerId);
        assertThat(rideSnapshot.getPeriod().getStartTime()).isEqualTo(LocalDateTime.now(clock));

    }

    @Test
    public void shouldThrowExceptionWhenTryToCreateRideForNotActiveUser() {
        //given
        PassengerId passengerId = PassengerId.random();
        CrmFacade crmFacade = mock(CrmFacade.class);
        when(crmFacade.isUserActive(passengerId)).thenReturn(false);
        RideFactory rideFactory = new RideFactory(
                crmFacade,
                mock(DriverRepository.class),
                new DomainTools(
                        mock(EventPublisher.class),
                        Clock.systemUTC()
                )
        );

        //when
        assertThatExceptionOfType(UserNotActiveException.class).isThrownBy(() -> rideFactory.create(passengerId, DriverId
                .random(), 0, 1));
    }
}
