// package com.pietaxi.rides.domain.model;
//
// import java.util.Arrays;
// import java.util.List;
//
// import org.junit.Test;
//
// import com.pietaxi.rides.application.CannotFindSuitableDriver;
// import com.pietaxi.rides.application.RequestRideCommand;
//
// import static org.assertj.core.api.Assertions.assertThat;
// import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
//
// /**
//  * @author pawel szymczyk
//  */
// public class FreeDriversCatalogTest {
//     private static final RideSpecificationFactory RIDE_SPECIFICATION_FACTORY = new RideSpecificationFactory();
//
//     private static final Driver BUSINESS_LINE_DRIVER = new Driver("123").registerFeature(new BusinessLineFeature());
//     private static final Driver ELECTRIC_LINE_DRIVER = new Driver("456").registerFeature(new ElectricCarFeature());
//     private static final Driver ELECTRIC_AND_BUSINESS_LINE_DRIVER = new Driver("789")
//             .registerFeature(new BusinessLineFeature())
//             .registerFeature(new ElectricCarFeature());
//
//     @Test
//     public void shouldFindDefaultDriver() {
//         //given
//         String[] ids = {"123", "456", "789"};
//         List<Driver> drivers = Arrays.asList(
//                 ELECTRIC_LINE_DRIVER,
//                 ELECTRIC_AND_BUSINESS_LINE_DRIVER,
//                 new Driver(ids[0]),
//                 new Driver(ids[1]),
//                 new Driver(ids[2]));
//         FreeDriversCatalog freeDriversCatalog = new FreeDriversCatalog(drivers);
//
//         //when
//         Driver selected = freeDriversCatalog.selectSuitable(RIDE_SPECIFICATION_FACTORY.create(new RequestRideCommand(false, false)));
//
//         //then
//         assertThat(ids).contains(selected.getLicenceNumber());
//     }
//
//     @Test
//     public void shouldFindBusinessLineDriver() {
//         //given
//         List<Driver> drivers = Arrays.asList(
//                 new Driver("564"),
//                 ELECTRIC_LINE_DRIVER,
//                 new Driver("737"),
//                 new Driver("463"),
//                 BUSINESS_LINE_DRIVER
//         );
//         FreeDriversCatalog freeDriversCatalog = new FreeDriversCatalog(drivers);
//
//         //when
//         Driver selected = freeDriversCatalog.selectSuitable(RIDE_SPECIFICATION_FACTORY.create(new RequestRideCommand(true, false)));
//
//         //then
//         assertThat(selected.getId()).isEqualTo(BUSINESS_LINE_DRIVER.getId());
//     }
//
//     @Test
//     public void shouldFindDriverWithElectricCar() {
//         //given
//         List<Driver> drivers = Arrays.asList(
//                 new Driver("963"),
//                 new Driver("973"),
//                 BUSINESS_LINE_DRIVER,
//                 new Driver("523"),
//                 ELECTRIC_LINE_DRIVER
//         );
//         FreeDriversCatalog freeDriversCatalog = new FreeDriversCatalog(drivers);
//
//         //when
//         Driver selected = freeDriversCatalog.selectSuitable(RIDE_SPECIFICATION_FACTORY.create(new RequestRideCommand(false, true)));
//
//         //then
//         assertThat(selected.getId()).isEqualTo(ELECTRIC_LINE_DRIVER.getId());
//     }
//
//     @Test
//     public void shouldFindDriverWithBusinessLineAndElectricCar() {
//         //given
//         List<Driver> drivers = Arrays.asList(
//                 new Driver("575"),
//                 new Driver("963"),
//                 BUSINESS_LINE_DRIVER,
//                 new Driver("932"),
//                 ELECTRIC_LINE_DRIVER,
//                 ELECTRIC_AND_BUSINESS_LINE_DRIVER
//         );
//         FreeDriversCatalog freeDriversCatalog = new FreeDriversCatalog(drivers);
//
//         //when
//         Driver selected = freeDriversCatalog.selectSuitable(RIDE_SPECIFICATION_FACTORY.create(new RequestRideCommand(true, true)));
//
//         //then
//         assertThat(selected.getId()).isEqualTo(ELECTRIC_AND_BUSINESS_LINE_DRIVER.getId());
//     }
//
//     @Test
//     public void shouldThrowExceptionWhenCannotFindSuitableDriver() {
//         //given
//         List<Driver> drivers = Arrays.asList(
//                 new Driver("142"),
//                 new Driver("963"),
//                 ELECTRIC_LINE_DRIVER,
//                 BUSINESS_LINE_DRIVER);
//         FreeDriversCatalog freeDriversCatalog = new FreeDriversCatalog(drivers);
//
//         //expect
//         assertThatExceptionOfType(CannotFindSuitableDriver.class)
//                 .isThrownBy(() -> freeDriversCatalog.selectSuitable(RIDE_SPECIFICATION_FACTORY.create(new RequestRideCommand(true, true))));
//     }
//
// }