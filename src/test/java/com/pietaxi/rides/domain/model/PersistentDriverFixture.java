// package com.pietaxi.rides.domain.model;
//
// import java.util.UUID;
//
// /**
//  * @author pawel szymczyk
//  */
// public class PersistentDriverFixture {
//
//     private final DriverRepository driverRepository;
//
//     public PersistentDriverFixture(DriverRepository driverRepository) {
//         this.driverRepository = driverRepository;
//     }
//
//     public UUID aDriver() {
//         Driver driver = new Driver("123");
//         driverRepository.save(driver);
//         return driver.getId();
//     }
//
//     public UUID aDriverWithElectricCar() {
//         Driver driver = new Driver("123").registerFeature(new ElectricCarFeature());
//         driverRepository.save(driver);
//         return driver.getId();
//     }
//
//     public UUID aDriverWithBusinessLine() {
//         Driver driver = new Driver("123").registerFeature(new BusinessLineFeature());
//         driverRepository.save(driver);
//         return driver.getId();
//     }
//
//     public UUID aDriverWithBusinessLineAndElectric() {
//         Driver driver = new Driver("123")
//                 .registerFeature(new BusinessLineFeature())
//                 .registerFeature(new ElectricCarFeature());
//
//         driverRepository.save(driver);
//         return driver.getId();
//     }
//
//     public void clearDriversCatalog() {
//         for (Driver driver: driverRepository.findAllFreeDrivers()) {
//             driverRepository.delete(driver);
//         }
//     }
// }
