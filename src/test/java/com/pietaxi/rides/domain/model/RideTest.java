package com.pietaxi.rides.domain.model;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.Queue;

import org.junit.Test;

import com.pietaxi.common.ddd.DomainTools;
import com.pietaxi.common.ddd.event.DomainEvent;
import com.pietaxi.common.ddd.event.EventPublisher;
import com.pietaxi.common.ddd.event.NoOperationEventPublisher;
import com.pietaxi.common.ddd.vo.Period;
import com.pietaxi.sharedkernel.DriverId;
import com.pietaxi.sharedkernel.PassengerId;
import com.pietaxi.sharedkernel.RideId;

import static java.time.LocalDateTime.now;
import static java.time.ZoneOffset.UTC;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

/**
 * @author pawel szymczyk
 */
public class RideTest {

    private static final EventPublisher noOperationEventPublisher = new NoOperationEventPublisher();
    private static final Clock clock = Clock.fixed(LocalDateTime.of(2000, 1, 1, 1, 1).toInstant(UTC), UTC);

    private static final DriverId dummyDriver = DriverId.random();
    private static final PassengerId dummyPassenger = PassengerId.random();
    private static final Location startLocation = new Location(1, 1);
    private static final Location endLocation = new Location(2, 2);

    @Test
    public void shouldFinishRide() {
        //given
        Ride ride = dummyRide();

        //when
        RideSnapshot rideSnapshot = ride.finish(endLocation);

        //then
        assertThat(ride.isFinished()).isTrue();
        assertThat(rideSnapshot.getRideId()).isEqualTo(ride.getId());
    }

    @Test
    public void shouldReturnCorrectPassengerDetails() {
        //given
        Ride ride = dummyRide();

        //when
        RideSnapshot rideSnapshot = ride.finish(endLocation);

        //then
        assertThat(rideSnapshot.getPassengerId()).isEqualTo(dummyPassenger);
    }

    @Test
    public void shouldReturnCorrectDriverDetails() {
        //given
        Ride ride = dummyRide();

        //when
        RideSnapshot rideSnapshot = ride.finish(endLocation);

        //then
        assertThat(rideSnapshot.getDriverId()).isEqualTo(dummyDriver);
    }

    @Test
    public void shouldCalculateRidePeriod() {
        //given
        LocalDateTime startTime = now(clock).minusHours(1);
        Ride ride = rideStartedAt(startTime);

        //when
        RideSnapshot rideSnapshot = ride.finish(endLocation);

        //then
        assertThat(rideSnapshot.getPeriod()).isEqualTo(new Period(startTime, now(clock)));
    }

    @Test
    public void shouldCalculateRideDistance() {
        //given
        Location startLocation = new Location(52, 21);
        Location targetLocation = new Location(21, 18);

        //when
        RideSnapshot rideSnapshot = rideStartedAt(startLocation).finish(targetLocation);

        //then
        assertThat(rideSnapshot.distanceInKm()).isEqualTo(31L);
    }

    @Test
    public void shouldNotAllowToFinishRideTwice() {
        //given
        Ride ride = dummyRide();

        //when
        ride.finish(endLocation);

        //then
        assertThatExceptionOfType(CannotFinishAlreadyFinishedRide.class).isThrownBy(() -> ride.finish(endLocation));
    }

    @Test
    public void shouldSendEventWhenFinishedRide() {
        //given
        final Queue<DomainEvent> events = new LinkedList<>();
        EventPublisher mockEventPublisher = events::add;

        Ride ride = dummyRideWithEventPublisher(mockEventPublisher);

        //when
        ride.finish(endLocation);

        //then
        assertThat(events)
                .filteredOn(RideFinished.class::isInstance)
                .hasSize(1)
                .extracting(RideFinished.class::cast)
                .first()
                .hasFieldOrPropertyWithValue("rideId", ride.getId());
    }

    private Ride dummyRide() {
        return new Ride(RideId.random(), dummyDriver, dummyPassenger, startLocation, now(clock), new DomainTools(noOperationEventPublisher, clock));
    }

    private Ride rideStartedAt(LocalDateTime startTime) {
        return new Ride(RideId.random(), dummyDriver, dummyPassenger, startLocation, startTime, new DomainTools(noOperationEventPublisher, clock));
    }

    private Ride rideStartedAt(Location startLocation) {
        return new Ride(RideId.random(), dummyDriver, dummyPassenger, startLocation, now(clock), new DomainTools(noOperationEventPublisher, clock));
    }

    private Ride dummyRideWithEventPublisher(EventPublisher eventPublisher) {
        return new Ride(RideId.random(), dummyDriver, dummyPassenger, startLocation, now(clock), new DomainTools(eventPublisher, clock));
    }
}
