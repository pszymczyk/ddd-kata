package com.pietaxi.rides.domain.model;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author pawel szymczyk
 */
@RunWith(JUnitParamsRunner.class)
public class RouteTest {

    @Test
    @Parameters({
            "0, 0, 0, 0, 0",
            "1, 1, 1, 1, 0",
            "1, 1, 2, 2, 1",
            "1, 1, 9, 3, 8",
            "19, 99, 2, 3, 17"
    })
    public void shouldCalculateSimpleDistanceBetweenTwoPoints(long x1, long y1, long x2, long y2, long distance) {

        //expect
        assertThat(new Route(new Location(x1, y1), new Location(x2, y2)).getDistanceInKm()).isEqualTo(distance);
    }

}