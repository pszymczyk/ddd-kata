package com.pietaxi.rides.domain.model;

import java.time.LocalDateTime;

import com.pietaxi.common.ddd.DomainTools;
import com.pietaxi.sharedkernel.DriverId;
import com.pietaxi.sharedkernel.PassengerId;
import com.pietaxi.sharedkernel.RideId;

public class PersistentRideFixture {

    private final RidesRepository ridesRepository;
    private final DomainTools domainTools;

    public PersistentRideFixture(RidesRepository ridesRepository,
            DomainTools domainTools) {
        this.ridesRepository = ridesRepository;
        this.domainTools = domainTools;
    }

    public Ride dummyPresentRide() {
        DriverId driver = DriverId.random();
        Ride ride = new Ride(
                RideId.random(),
                driver,
                PassengerId.random(),
                new Location(10, 10),
                LocalDateTime.now(),
                domainTools
        );

        ridesRepository.save(ride);
        return ride;
    }
}
