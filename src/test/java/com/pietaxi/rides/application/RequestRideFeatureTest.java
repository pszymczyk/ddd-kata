// package com.pietaxi.rides.application;
//
// import com.pietaxi.common.CommonConfiguration;
// import com.pietaxi.rides.TestConfiguration;
// import com.pietaxi.rides.domain.model.RideOffer;
// import com.pietaxi.rides.domain.model.PersistentDriverFixture;
// import com.pietaxi.rides.fixtures.PersistentDriverFixtureConfiguration;
// import com.pietaxi.rides.port.adapter.mail.RidesERPAdapterConfiguration;
// import com.pietaxi.rides.port.adapter.memory.RidesMemoryAdapterConfiguration;
// import org.junit.Before;
// import org.junit.Test;
// import org.junit.runner.RunWith;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.test.context.junit4.SpringRunner;
//
// import java.util.ArrayList;
// import java.util.List;
// import java.util.UUID;
//
// import static org.assertj.core.api.Assertions.assertThat;
// import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
//
// /**
//  * @author pawel szymczyk
//  */
// @RunWith(SpringRunner.class)
// @SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
//         classes = {
//                 RidesApiConfiguration.class,
//                 PersistentDriverFixtureConfiguration.class,
//                 RidesERPAdapterConfiguration.class,
//                 RidesMemoryAdapterConfiguration.class,
//                 CommonConfiguration.class,
//                 TestConfiguration.class})
// @EnableAutoConfiguration
// public class RequestRideFeatureTest {
//
//     @Autowired
//     RequestRideFeature requestRideFeature;
//
//     @Autowired
//     PersistentDriverFixture persistentDriverFixture;
//
//     @Before
//     public void setup() {
//         persistentDriverFixture.clearDriversCatalog();
//     }
//
//     @Test
//     public void shouldFindDefaultDriver() {
//         //given
//         List<UUID> ids = new ArrayList<>();
//         ids.add(persistentDriverFixture.aDriverWithElectricCar());
//         ids.add(persistentDriverFixture.aDriverWithBusinessLine());
//         ids.add(persistentDriverFixture.aDriver());
//         ids.add(persistentDriverFixture.aDriver());
//         ids.add(persistentDriverFixture.aDriver());
//
//         //when
//         RideOffer rideOffer = requestRideFeature.requestRide(new RequestRideCommand(false, false));
//
//         //then
//         assertThat(ids).contains(rideOffer.getDriverId());
//     }
//
//     @Test
//     public void shouldFindBusinessLineDriver() {
//         //given
//         persistentDriverFixture.aDriver();
//         persistentDriverFixture.aDriverWithElectricCar();
//         persistentDriverFixture.aDriver();
//         persistentDriverFixture.aDriver();
//         UUID businessLineDriverId = persistentDriverFixture.aDriverWithBusinessLine();
//
//         //when
//         RideOffer rideOffer = requestRideFeature.requestRide(new RequestRideCommand(true, false));
//
//         //then
//         assertThat(rideOffer.getDriverId()).isEqualTo(businessLineDriverId);
//     }
//
//     @Test
//     public void shouldFindDriverWithElectricCar() {
//         //given
//         persistentDriverFixture.aDriver();
//         persistentDriverFixture.aDriver();
//         persistentDriverFixture.aDriverWithBusinessLine();
//         persistentDriverFixture.aDriver();
//         UUID electricLineDriverId = persistentDriverFixture.aDriverWithElectricCar();
//
//         //when
//         RideOffer rideOffer = requestRideFeature.requestRide(new RequestRideCommand(false, true));
//
//         //then
//         assertThat(rideOffer.getDriverId()).isEqualTo(electricLineDriverId);
//     }
//
//     @Test
//     public void shouldFindDriverWithBusinessLineAndElectricCar() {
//         //given
//         persistentDriverFixture.aDriver();
//         persistentDriverFixture.aDriver();
//         persistentDriverFixture.aDriverWithBusinessLine();
//         persistentDriverFixture.aDriver();
//         persistentDriverFixture.aDriverWithBusinessLine();
//         UUID bussinessLineAndElectricDriverId = persistentDriverFixture.aDriverWithBusinessLineAndElectric();
//
//         //when
//         RideOffer rideOffer = requestRideFeature.requestRide(new RequestRideCommand(true, true));
//
//         //then
//         assertThat(rideOffer.getDriverId()).isEqualTo(bussinessLineAndElectricDriverId);
//     }
//
//     @Test
//     public void shouldThrowExceptionWhenCannotFindSuitableDriver() {
//         //given
//         persistentDriverFixture.aDriver();
//         persistentDriverFixture.aDriver();
//         persistentDriverFixture.aDriverWithBusinessLine();
//         persistentDriverFixture.aDriverWithElectricCar();
//
//         //expect
//         assertThatExceptionOfType(CannotFindSuitableDriver.class).isThrownBy(() -> requestRideFeature.requestRide(new RequestRideCommand(true, true)));
//     }
// }