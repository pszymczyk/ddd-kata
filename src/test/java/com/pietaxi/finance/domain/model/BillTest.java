package com.pietaxi.finance.domain.model;

import org.junit.Test;

import com.pietaxi.common.ddd.vo.Money;
import com.pietaxi.common.ddd.vo.Period;
import com.pietaxi.sharedkernel.PassengerId;

import static java.time.LocalDateTime.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author pawel szymczyk
 */
public class BillTest {

    @Test
    public void shouldCalculateFeesForGivenBillingStrategy() {
        //given
        Money expectedFee = Money.of("122");
        RideSummary rideSummary = new RideSummary(123, new Period(now(), now().plusMinutes(10)));
        BillingStrategy billingStrategy = mock(BillingStrategy.class);
        when(billingStrategy.calculate(rideSummary)).thenReturn(expectedFee);
        Bill bill = new Bill(BillId.random(), PassengerId.random(), billingStrategy, rideSummary.getPeriod(), rideSummary.getDistanceInKm());

        //when
        Money fee = bill.account();

        //then
        assertThat(fee).isEqualTo(expectedFee);
    }
}
