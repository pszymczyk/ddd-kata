package com.pietaxi.finance.domain.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.pietaxi.finance.port.adapter.InMemoryBillingStrategyRepository;
import com.pietaxi.finance.port.adapter.InMemoryDriverRepository;
import com.pietaxi.rides.RidesFacade;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RunWith(MockitoJUnitRunner.class)
public class BillFactoryTest {

    @Mock
    RidesFacade ridesFacade;

    BillFactory billFactory;

    @Before
    public void before() {
        billFactory = new BillFactory(ridesFacade, new InMemoryDriverRepository(), new InMemoryBillingStrategyRepository());
    }

    @Test
    public void shouldThrowExceptionWhenTryToCreateRideWithoutId() {
        assertThatThrownBy(() -> billFactory.create(null)).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void shouldCreateRideWithDefaultBillingStrategy() {

    }

    @Test
    public void shouldCreateRideWithBillingStrategySelectedForGivenDriver() {
    }
}
