package com.pietaxi.finance;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.pietaxi.common.CommonConfiguration;
import com.pietaxi.common.ddd.event.EventPublisher;
import com.pietaxi.common.ddd.vo.Period;
import com.pietaxi.finance.port.adapter.FinanceAdaptersConfiguration;
import com.pietaxi.rides.RideDto;
import com.pietaxi.rides.RidesFacade;
import com.pietaxi.rides.domain.model.RideFinished;
import com.pietaxi.sharedkernel.DriverId;
import com.pietaxi.sharedkernel.PassengerId;
import com.pietaxi.sharedkernel.RideId;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {
                FinanceAdaptersConfiguration.class,
                FinanceIntegrationTest.Config.class,
                CommonConfiguration.class,
        },
        properties = "erp.url=http://localhost:8090/super-erp"
)
@EnableAutoConfiguration
public class FinanceIntegrationTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(WireMockConfiguration.options().port(8090));

    @Autowired
    EventPublisher eventPublisher;

    @Autowired
    RidesFacade ridesFacade;

    @Test
    public void shouldChargeUser() {
        //given
        RideId rideId = RideId.random();
        PassengerId passengerId = PassengerId.random();
        when(ridesFacade.getRideDetails(rideId)).thenReturn(Optional.of(new RideDto(
                DriverId.random(),
                passengerId,
                new Period(LocalDateTime.now(), LocalDateTime.now().plusMinutes(10)),
                100
        )));
        wireMockRule.stubFor(post(urlEqualTo("/super-erp"))
                .willReturn(aResponse().withStatus(201)));

        //when
        eventPublisher.publishEvent(new RideFinished(LocalDateTime.now(), rideId));

        //then
        verify(postRequestedFor(urlEqualTo("/super-erp"))
                .withRequestBody(containing(passengerId.asString())));

    }

    @Configuration
    static class Config {

        @Bean
        RidesFacade ridesFacade() {
            return mock(RidesFacade.class);
        }
    }
}
