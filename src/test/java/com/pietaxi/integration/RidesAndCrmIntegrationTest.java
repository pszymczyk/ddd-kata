package com.pietaxi.integration;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.pietaxi.common.CommonConfiguration;
import com.pietaxi.common.ddd.DomainTools;
import com.pietaxi.common.ddd.event.EventPublisher;
import com.pietaxi.common.ddd.vo.Email;
import com.pietaxi.crm.CrmFacadeConfiguration;
import com.pietaxi.crm.domain.model.Address;
import com.pietaxi.crm.domain.model.User;
import com.pietaxi.crm.domain.model.UserId;
import com.pietaxi.crm.domain.model.UserRepository;
import com.pietaxi.crm.port.adapter.CrmAdaptersConfiguration;
import com.pietaxi.rides.RidesFacadeConfiguration;
import com.pietaxi.rides.application.RidesApplicationConfiguration;
import com.pietaxi.rides.domain.model.Location;
import com.pietaxi.rides.domain.model.Ride;
import com.pietaxi.rides.domain.model.RidesRepository;
import com.pietaxi.rides.port.adapter.rest.RidesRestAdapter;
import com.pietaxi.rides.port.adapter.rest.RidesRestAdapterConfiguration;
import com.pietaxi.rides.port.adapter.sql.RidesSqlAdapterConfiguration;
import com.pietaxi.sharedkernel.DriverId;
import com.pietaxi.sharedkernel.PassengerId;
import com.pietaxi.sharedkernel.RideId;

import static org.awaitility.Awaitility.await;

/**
 * @author pawel szymczyk
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {
                CrmAdaptersConfiguration.class,
                CrmFacadeConfiguration.class,
                RidesRestAdapterConfiguration.class,
                RidesSqlAdapterConfiguration.class,
                RidesApplicationConfiguration.class,
                RidesFacadeConfiguration.class,
                CommonConfiguration.class,
                IntegrationConfiguration.class })
@EnableAutoConfiguration
public class RidesAndCrmIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private RidesRepository ridesRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EventPublisher eventPublisher;

    @Autowired
    private Clock clock;

    @Autowired
    private MockSurveySender mockSurveySender;

    @Test
    public void ridesShouldNotifyCrmAboutRideFinished() {
        //given
        PassengerId passengerId = PassengerId.random();
        Ride ride = new Ride(
                RideId.random(),
                DriverId.random(),
                passengerId,
                new Location(0, 0),
                LocalDateTime.now(clock),
                new DomainTools(eventPublisher,
                        clock));
        ridesRepository.save(ride);

        Email email = Email.parse("kazik@wp.pl");
        User user = new User(new UserId("123"), passengerId, "Kazik", email, Address.fromString("Kowalowa 47 \n 04-021 Warszawa"));
        userRepository.save(user);

        RidesRestAdapter.FinishRideRequest finishRideRequest = new RidesRestAdapter.FinishRideRequest();
        finishRideRequest.setRideId(ride.getId().asString());
        finishRideRequest.setLongitude(10L);
        finishRideRequest.setLatitude(10L);

        //when
        ResponseEntity<?> response = restTemplate.postForEntity("/finishedrides", finishRideRequest, Void.class);

        //then
        await().atMost(10, TimeUnit.SECONDS).ignoreExceptions().untilAsserted(() -> {
            Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
            Assertions.assertThat(mockSurveySender.lastSentMessageEmail()).isEqualTo(email);
        });
    }
}
