package com.pietaxi.integration;

import java.util.LinkedList;

import com.pietaxi.common.ddd.vo.Email;
import com.pietaxi.crm.domain.model.Survey;
import com.pietaxi.crm.domain.model.SurveySender;
import com.pietaxi.crm.domain.model.User;

class MockSurveySender implements SurveySender {

    private LinkedList<Email> emails = new LinkedList<>();

    @Override
    public void send(Survey survey, User user) {
        emails.addLast(user.getEmail());
    }

    public Email lastSentMessageEmail() {
        return emails.getLast();
    }
}
