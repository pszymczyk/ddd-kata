package com.pietaxi.integration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
class IntegrationConfiguration {

    @Bean
    @Primary
    MockSurveySender surveySender() {
        return new MockSurveySender();
    }

}
