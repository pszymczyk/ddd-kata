package com.pietaxi.integration;

import java.time.Clock;
import java.time.LocalDateTime;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.pietaxi.common.CommonConfiguration;
import com.pietaxi.common.ddd.DomainTools;
import com.pietaxi.common.ddd.event.EventPublisher;
import com.pietaxi.crm.CrmFacade;
import com.pietaxi.finance.port.adapter.FinanceAdaptersConfiguration;
import com.pietaxi.rides.RidesFacadeConfiguration;
import com.pietaxi.rides.application.RidesApplicationConfiguration;
import com.pietaxi.rides.domain.model.Location;
import com.pietaxi.rides.domain.model.Ride;
import com.pietaxi.rides.domain.model.RidesRepository;
import com.pietaxi.rides.port.adapter.rest.RidesRestAdapter;
import com.pietaxi.rides.port.adapter.rest.RidesRestAdapterConfiguration;
import com.pietaxi.rides.port.adapter.sql.RidesSqlAdapterConfiguration;
import com.pietaxi.sharedkernel.DriverId;
import com.pietaxi.sharedkernel.PassengerId;
import com.pietaxi.sharedkernel.RideId;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author pawel szymczyk
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {
                FinanceAdaptersConfiguration.class,
                RidesRestAdapterConfiguration.class,
                RidesSqlAdapterConfiguration.class,
                RidesApplicationConfiguration.class,
                RidesFacadeConfiguration.class,
                CommonConfiguration.class,
                IntegrationConfiguration.class,
                RidesAndFinanceIntegrationTest.Config.class
        },
        properties = "erp.url=http://localhost:8090/super-erp")
@EnableAutoConfiguration
public class RidesAndFinanceIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private RidesRepository ridesRepository;

    @Autowired
    private EventPublisher eventPublisher;

    @Autowired
    private Clock clock;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(WireMockConfiguration.options().port(8090));

    @Test
    public void ridesShouldNotifyFinanceAboutRideFinished() {
        //given
        PassengerId passengerId = PassengerId.random();
        Ride ride = new Ride(
                RideId.random(),
                DriverId.random(),
                passengerId,
                new Location(0, 0),
                LocalDateTime.now(clock),
                new DomainTools(eventPublisher,
                        clock));
        ridesRepository.save(ride);

        RidesRestAdapter.FinishRideRequest finishRideRequest = new RidesRestAdapter.FinishRideRequest();
        finishRideRequest.setRideId(ride.getId().asString());
        finishRideRequest.setLongitude(10L);
        finishRideRequest.setLatitude(10L);

        wireMockRule.stubFor(post(urlEqualTo("/super-erp"))
                .willReturn(aResponse().withStatus(201)));

        //when
        ResponseEntity<?> response = restTemplate.postForEntity("/finishedrides", finishRideRequest, Void.class);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(postRequestedFor(urlEqualTo("/super-erp")).withRequestBody(containing(passengerId.asString())));
    }

    @Configuration
    static class Config {

        @Bean
        CrmFacade crmFacade() {
            CrmFacade crmFacade = mock(CrmFacade.class);
            when(crmFacade.isUserActive(any(PassengerId.class))).thenReturn(true);
            return crmFacade;
        }
    }
}
