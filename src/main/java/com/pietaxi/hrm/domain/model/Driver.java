package com.pietaxi.hrm.domain.model;

import java.util.UUID;

import com.pietaxi.common.ddd.Entity;

/**
 * @author pawel szymczyk
 */
public class Driver extends Entity {

    private enum Status {
        ACTIVE,
        SUSPENDED,
    }

    private Status status = Status.ACTIVE;

    private final String licenceNumber;

    public Driver(UUID id, String licenceNumber) {
        super(id);
        this.licenceNumber = licenceNumber;
    }

    void suspend() {
        status = Status.SUSPENDED;
    }

    boolean isSuspended() {
        return status == Status.SUSPENDED;
    }

}
