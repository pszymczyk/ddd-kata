package com.pietaxi.rides.port.adapter.memory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pietaxi.rides.domain.model.DriverRepository;
import com.pietaxi.rides.domain.model.HistoricalRidesRepository;
import com.pietaxi.rides.domain.model.RidesRepository;

/**
 * @author pawel szymczyk
 */
@Configuration
public class RidesMemoryAdapterConfiguration {

    @Bean
    public HistoricalRidesRepository finishedRidesRepository() {
        return new InMemoryFinishedRidesRepository();
    }

    @Bean
    public RidesRepository ridesRepository() {
        return new InMemoryRidesRepository();
    }

    @Bean
    public DriverRepository ridesDriverRepository() {
        return new InMemoryDriverRepository();
    }

}
