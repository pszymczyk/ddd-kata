package com.pietaxi.rides.port.adapter.sql;

import java.time.LocalDateTime;

import com.pietaxi.common.ddd.DomainTools;
import com.pietaxi.rides.domain.model.Location;
import com.pietaxi.rides.domain.model.Ride;
import com.pietaxi.rides.domain.model.RidesRepository;
import com.pietaxi.sharedkernel.DriverId;
import com.pietaxi.sharedkernel.PassengerId;
import com.pietaxi.sharedkernel.RideId;

class SqlRideRepository implements RidesRepository {

    private final CrudRideRepository crudRideRepository;
    private final DomainTools domainTools;

    SqlRideRepository(CrudRideRepository crudRideRepository, DomainTools domainTools) {
        this.crudRideRepository = crudRideRepository;
        this.domainTools = domainTools;
    }

    @Override
    public Ride findById(RideId rideId) {
        RideEntity entity = crudRideRepository.findByRideId(rideId.asString());

        String[] location = entity.getStartLocation().split(",");

        return new Ride(
                RideId.from(entity.getRideId()),
                DriverId.from(entity.getDriverId()),
                PassengerId.from(entity.getPassengerId()),
                new Location(Long.valueOf(location[0]), Long.valueOf(location[1])),
                LocalDateTime.parse(entity.getStartTime()),
                domainTools
        );
    }

    @Override
    public void save(Ride ride) {
        RideEntity rideEntity = new RideEntity();
        rideEntity.setRideId(ride.getId().asString());
        rideEntity.setDriverId(ride.getDriverId().asString());
        rideEntity.setPassengerId(ride.getPassengerId().asString());
        rideEntity.setStartLocation(ride.getStartLocation().asString());
        rideEntity.setStartTime(ride.getStartTime().toString());
        rideEntity.setStatus(ride.getStatus().name());
        crudRideRepository.save(rideEntity);
    }

    @Override
    public void delete(Ride ride) {
        RideEntity entity = crudRideRepository.findByRideId(ride.getId().asString());
        crudRideRepository.delete(entity);
    }
}
