package com.pietaxi.rides.port.adapter.sql;

import org.springframework.data.repository.CrudRepository;

interface CrudRideRepository extends CrudRepository<RideEntity, Long>  {

    RideEntity findByRideId(String rideId);
}
