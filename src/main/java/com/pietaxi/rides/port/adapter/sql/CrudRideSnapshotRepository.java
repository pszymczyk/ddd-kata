package com.pietaxi.rides.port.adapter.sql;

import org.springframework.data.repository.CrudRepository;

interface CrudRideSnapshotRepository extends CrudRepository<RideSnapshotEntity, Long>  {

    RideSnapshotEntity findByRideId(String rideId);
}
