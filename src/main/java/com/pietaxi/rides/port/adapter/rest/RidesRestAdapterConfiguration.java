package com.pietaxi.rides.port.adapter.rest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pietaxi.rides.application.FinishRideFeature;
import com.pietaxi.rides.application.StartRideFeature;

/**
 * @author pawel szymczyk
 */
@Configuration
public class RidesRestAdapterConfiguration {

    @Bean
    RidesRestAdapter ridesRestAdapter(FinishRideFeature finishRideFeature, StartRideFeature startRideFeature) {
        return new RidesRestAdapter(finishRideFeature, startRideFeature);
    }
}
