package com.pietaxi.rides.port.adapter.memory;

import java.util.HashMap;
import java.util.Map;

import com.pietaxi.rides.domain.model.Ride;
import com.pietaxi.rides.domain.model.RidesRepository;
import com.pietaxi.sharedkernel.RideId;

/**
 * @author pawel szymczyk
 */
public class InMemoryRidesRepository implements RidesRepository {

    private Map<RideId, Ride> storage = new HashMap<>();

    @Override
    public Ride findById(RideId rideId) {
        return storage.get(rideId);
    }

    @Override
    public void save(Ride ride) {
        storage.put(ride.getId(), ride);
    }

    @Override
    public void delete(Ride ride) {
        ride.markAsRemoved();
    }
}
