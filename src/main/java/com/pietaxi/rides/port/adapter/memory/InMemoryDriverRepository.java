package com.pietaxi.rides.port.adapter.memory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pietaxi.rides.domain.model.Driver;
import com.pietaxi.rides.domain.model.DriverRepository;
import com.pietaxi.sharedkernel.DriverId;

/**
 * @author pawel szymczyk
 */
public class InMemoryDriverRepository implements DriverRepository {

    private static final Logger log = LoggerFactory.getLogger(InMemoryDriverRepository.class);
    private static final Map<DriverId, Driver> storage = new HashMap<>();

    static {
        IntStream.range(0, 10)
                 .forEach(i -> {
                     Driver driver = new Driver(DriverId.random());
                     storage.put(driver.getId(), driver);
                     log.info("Adding test drivers to database, passengerId " + driver.getId());
                 });
    }

    @Override
    public void save(Driver driver) {
        storage.put(driver.getId(), driver);
    }

    @Override
    public Driver findById(DriverId driverId) {
        return storage.get(driverId);
    }
    @Override
    public List<Driver> findAllFreeDrivers() {
        List<Driver> drivers = new ArrayList<>();
        for (Driver d: storage.values()) {
            if (!d.isRemoved()) {
                drivers.add(d);
            }
        }

        return drivers;
    }
}
