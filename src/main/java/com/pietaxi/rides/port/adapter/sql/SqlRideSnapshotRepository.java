package com.pietaxi.rides.port.adapter.sql;

import java.time.LocalDateTime;

import com.pietaxi.common.ddd.vo.Period;
import com.pietaxi.rides.domain.model.HistoricalRidesRepository;
import com.pietaxi.rides.domain.model.Location;
import com.pietaxi.rides.domain.model.RideSnapshot;
import com.pietaxi.rides.domain.model.Route;
import com.pietaxi.sharedkernel.DriverId;
import com.pietaxi.sharedkernel.PassengerId;
import com.pietaxi.sharedkernel.RideId;

import static java.lang.Long.valueOf;

public class SqlRideSnapshotRepository implements HistoricalRidesRepository {

    private final CrudRideSnapshotRepository crudRideSnapshotRepository;

    public SqlRideSnapshotRepository(CrudRideSnapshotRepository crudRideSnapshotRepository) {
        this.crudRideSnapshotRepository = crudRideSnapshotRepository;
    }

    @Override
    public void save(RideSnapshot rideSnapshot) {
        RideSnapshotEntity rideSnapshotEntity = new RideSnapshotEntity();
        rideSnapshotEntity.setDriverId(rideSnapshot.getDriverId().asString());
        rideSnapshotEntity.setPassengerId(rideSnapshot.getPassengerId().asString());
        rideSnapshotEntity.setRideId(rideSnapshot.getRideId().asString());
        rideSnapshotEntity.setStartTime(rideSnapshot.getPeriod().getStartTime().toString());
        rideSnapshotEntity.setEndTime(rideSnapshot.getPeriod().getEndTime().toString());
        rideSnapshotEntity.setStartLocation(rideSnapshot.getRoute().getStartLocation().asString());
        rideSnapshotEntity.setTargetLocation(rideSnapshot.getRoute().getTargetLocation().asString());
        crudRideSnapshotRepository.save(rideSnapshotEntity);
    }

    @Override
    public RideSnapshot findById(RideId rideId) {
        RideSnapshotEntity entity = crudRideSnapshotRepository.findByRideId(rideId.asString());

        String[] startLocation = entity.getStartLocation().split(",");
        String[] targetLocation = entity.getTargetLocation().split(",");

        return new RideSnapshot(
                RideId.from(entity.getRideId()),
                DriverId.from(entity.getDriverId()),
                PassengerId.from(entity.getPassengerId()),
                new Period(LocalDateTime.parse(entity.getStartTime()), LocalDateTime.parse(entity.getEndTime())),
                new Route(
                        new Location(valueOf(startLocation[0]), valueOf(startLocation[1])),
                        new Location(valueOf(targetLocation[0]), valueOf(targetLocation[1])))
        );
    }
}
