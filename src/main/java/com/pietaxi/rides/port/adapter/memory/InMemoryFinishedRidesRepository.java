package com.pietaxi.rides.port.adapter.memory;

import java.util.HashMap;
import java.util.Map;

import com.pietaxi.rides.domain.model.HistoricalRidesRepository;
import com.pietaxi.rides.domain.model.RideSnapshot;
import com.pietaxi.sharedkernel.RideId;

/**
 * @author pawel szymczyk
 */
public class InMemoryFinishedRidesRepository implements HistoricalRidesRepository {

    private final Map<RideId, RideSnapshot> storage = new HashMap<>();

    @Override
    public void save(RideSnapshot rideSnapshot) {
        storage.put(rideSnapshot.getRideId(), rideSnapshot);
    }

    @Override
    public RideSnapshot findById(RideId rideId) {
        return storage.get(rideId);
    }
}
