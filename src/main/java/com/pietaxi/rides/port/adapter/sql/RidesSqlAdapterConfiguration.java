package com.pietaxi.rides.port.adapter.sql;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.pietaxi.common.ddd.DomainTools;
import com.pietaxi.rides.domain.model.DriverRepository;
import com.pietaxi.rides.domain.model.HistoricalRidesRepository;
import com.pietaxi.rides.domain.model.RidesRepository;
import com.pietaxi.rides.port.adapter.memory.InMemoryDriverRepository;

/**
 * @author pawel szymczyk
 */
@Configuration
@EnableJpaRepositories( basePackageClasses = {CrudRideRepository.class, CrudRideSnapshotRepository.class})
@EntityScan("com.pietaxi.rides.port.adapter.sql")
public class RidesSqlAdapterConfiguration {

    @Bean
    public HistoricalRidesRepository finishedRidesRepository(CrudRideSnapshotRepository crudRideSnapshotRepository) {
        return new SqlRideSnapshotRepository(crudRideSnapshotRepository);
    }

    @Bean
    public DriverRepository ridesDriverRepository() {
        return new InMemoryDriverRepository();
    }

    @Bean
    public RidesRepository ridesRepository(CrudRideRepository crudRideRepository, DomainTools domainTools) {
        return new SqlRideRepository(crudRideRepository, domainTools);
    }

}
