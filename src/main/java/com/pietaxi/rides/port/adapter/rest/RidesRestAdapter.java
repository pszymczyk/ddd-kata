package com.pietaxi.rides.port.adapter.rest;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pietaxi.rides.application.FinishRideCommand;
import com.pietaxi.rides.application.FinishRideFeature;
import com.pietaxi.rides.application.StartRideCommand;
import com.pietaxi.rides.application.StartRideFeature;
import com.pietaxi.sharedkernel.DriverId;
import com.pietaxi.sharedkernel.PassengerId;
import com.pietaxi.sharedkernel.RideId;

/**
 * @author pawel szymczyk
 */
@RestController
public class RidesRestAdapter {

    private final FinishRideFeature finishRideFeature;
    private final StartRideFeature startRideFeature;

    RidesRestAdapter(FinishRideFeature finishRideFeature, StartRideFeature startRideFeature) {
        this.finishRideFeature = finishRideFeature;
        this.startRideFeature = startRideFeature;
    }

    public static class FinishRideRequest {

        private String rideId;
        private Long latitude;
        private Long longitude;

        public String getRideId() {
            return rideId;
        }

        public void setRideId(String rideId) {
            this.rideId = rideId;
        }

        public Long getLatitude() {
            return latitude;
        }

        public void setLatitude(Long latitude) {
            this.latitude = latitude;
        }

        public Long getLongitude() {
            return longitude;
        }

        public void setLongitude(Long longitude) {
            this.longitude = longitude;
        }
    }

    public static class StartRideRequest {

        private String passengerId;
        private String driverId;
        private Long pickLatitude;
        private Long pickLongitude;

        public String getPassengerId() {
            return passengerId;
        }

        public void setPassengerId(String passengerId) {
            this.passengerId = passengerId;
        }

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public Long getPickLatitude() {
            return pickLatitude;
        }

        public void setPickLatitude(Long pickLatitude) {
            this.pickLatitude = pickLatitude;
        }

        public Long getPickLongitude() {
            return pickLongitude;
        }

        public void setPickLongitude(Long pickLongitude) {
            this.pickLongitude = pickLongitude;
        }
    }

    @RequestMapping(path = "/finishedrides", method = RequestMethod.POST)
    public void finishRide(@RequestBody FinishRideRequest finishRideRequest) {
        finishRideFeature.finishRide(new FinishRideCommand(
                RideId.from(finishRideRequest.getRideId()),
                finishRideRequest.getLatitude(),
                finishRideRequest.getLongitude()
        ));
    }

    @RequestMapping(path = "/rides", method = RequestMethod.POST)
    public String Ride(@RequestBody StartRideRequest startRideRequest) {
        RideId rideId = startRideFeature.startRide(new StartRideCommand(
                PassengerId.from(startRideRequest.getPassengerId()),
                DriverId.from(startRideRequest.getDriverId()),
                startRideRequest.pickLatitude,
                startRideRequest.pickLongitude
        ));

        return rideId.asString();
    }


}
