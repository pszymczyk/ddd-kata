package com.pietaxi.rides;

import com.pietaxi.common.ddd.vo.Period;
import com.pietaxi.rides.domain.model.RideSnapshot;
import com.pietaxi.sharedkernel.DriverId;
import com.pietaxi.sharedkernel.PassengerId;

public class RideDto {

    private final DriverId driverId;
    private final PassengerId passengerId;
    private final Period period;
    private final long distanceInKm;

    RideDto(RideSnapshot rideSnapshot) {
        this(
                rideSnapshot.getDriverId(),
                rideSnapshot.getPassengerId(),
                rideSnapshot.getPeriod(),
                rideSnapshot.distanceInKm()
        );
    }

    public RideDto(DriverId driverId, PassengerId passengerId, Period period, long distanceInKm) {
        this.driverId = driverId;
        this.passengerId = passengerId;
        this.period = period;
        this.distanceInKm = distanceInKm;
    }

    public DriverId getDriverId() {
        return driverId;
    }

    public PassengerId getPassengerId() {
        return passengerId;
    }

    public Period getPeriod() {
        return period;
    }

    public long getDistanceInKm() {
        return distanceInKm;
    }
}
