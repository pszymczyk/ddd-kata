package com.pietaxi.rides.application;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pietaxi.common.ddd.DomainTools;
import com.pietaxi.crm.CrmFacade;
import com.pietaxi.rides.domain.model.DriverRepository;
import com.pietaxi.rides.domain.model.HistoricalRidesRepository;
import com.pietaxi.rides.domain.model.RideFactory;
import com.pietaxi.rides.domain.model.RideSpecificationFactory;
import com.pietaxi.rides.domain.model.RidesRepository;

/**
 * @author pawel szymczyk
 */
@Configuration
public class RidesApplicationConfiguration {

    @Bean
    public RequestRideFeature requestRideFeature(DriverRepository driverRepository) {
        RideSpecificationFactory rideSpecificationFactory = new RideSpecificationFactory();

        return new RequestRideFeature(rideSpecificationFactory, driverRepository);
    }

    @Bean
    public FinishRideFeature finishRideFeature(RidesRepository ridesRepository,
            HistoricalRidesRepository historicalRidesRepository) {

        return new FinishRideFeature(ridesRepository,
                historicalRidesRepository);
    }

    @Bean
    public StartRideFeature startRideFeature(DriverRepository driverRepository,
            RidesRepository ridesRepository,
            CrmFacade crmFacade,
            DomainTools domainTools) {
        RideFactory rideFactory = new RideFactory(
                crmFacade,
                driverRepository,
                domainTools);

        return new StartRideFeature(rideFactory, ridesRepository);
    }
}
