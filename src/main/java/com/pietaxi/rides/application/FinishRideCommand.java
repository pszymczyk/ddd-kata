package com.pietaxi.rides.application;

import com.pietaxi.sharedkernel.RideId;

/**
 * @author pawel szymczyk
 */
public class FinishRideCommand {

    private RideId rideId;
    private long targetLatitude;
    private long targetLongitude;

    public FinishRideCommand(RideId rideId,
                             long targetLatitude,
                             long targetLongitude) {
        this.rideId = rideId;
        this.targetLatitude = targetLatitude;
        this.targetLongitude = targetLongitude;
    }

    public RideId getRideId() {
        return rideId;
    }

    public long getTargetLatitude() {
        return targetLatitude;
    }

    public long getTargetLongitude() {
        return targetLongitude;
    }
}
