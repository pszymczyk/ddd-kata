package com.pietaxi.rides.application;

/**
 * @author pawel szymczyk
 */
public class RequestRideCommand {

    private final boolean businessLineRequired;
    private final boolean electricCarRequired;

    public RequestRideCommand(boolean businessLineRequired, boolean electricCarRequired) {
        this.businessLineRequired = businessLineRequired;
        this.electricCarRequired = electricCarRequired;
    }

    public boolean isBusinessLineRequired() {
        return businessLineRequired;
    }

    public boolean isElectricCarRequired() {
        return electricCarRequired;
    }
}
