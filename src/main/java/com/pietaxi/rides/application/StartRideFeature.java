package com.pietaxi.rides.application;

import com.pietaxi.rides.domain.model.Ride;
import com.pietaxi.rides.domain.model.RideFactory;
import com.pietaxi.rides.domain.model.RidesRepository;
import com.pietaxi.sharedkernel.RideId;

/**
 * @author pawel szymczyk
 */
public class StartRideFeature {

    private final RideFactory rideFactory;
    private final RidesRepository ridesRepository;

    public StartRideFeature(RideFactory rideFactory, RidesRepository ridesRepository) {
        this.rideFactory = rideFactory;
        this.ridesRepository = ridesRepository;
    }

    public RideId startRide(StartRideCommand startRideCommand) {
        Ride ride = rideFactory.create(
                startRideCommand.getPassengerId(),
                startRideCommand.getDriverId(),
                startRideCommand.getPickLatitude(),
                startRideCommand.getPickLongitude()
        );

        ridesRepository.save(ride);
        return ride.getId();
    }
}
