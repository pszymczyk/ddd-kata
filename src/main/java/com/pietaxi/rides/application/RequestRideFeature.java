package com.pietaxi.rides.application;

import com.pietaxi.common.ddd.specification.Specification;
import com.pietaxi.rides.domain.model.Driver;
import com.pietaxi.rides.domain.model.DriverRepository;
import com.pietaxi.rides.domain.model.FreeDriversCatalog;
import com.pietaxi.rides.domain.model.RideOffer;
import com.pietaxi.rides.domain.model.RideSpecificationFactory;

/**
 * @author pawel szymczyk
 */
public class RequestRideFeature {

    private final RideSpecificationFactory rideSpecificationFactory;
    private final DriverRepository driverRepository;

    public RequestRideFeature(RideSpecificationFactory rideSpecificationFactory, DriverRepository driverRepository) {
        this.rideSpecificationFactory = rideSpecificationFactory;
        this.driverRepository = driverRepository;
    }

    public RideOffer requestRide(RequestRideCommand requestRideCommand) {
        FreeDriversCatalog freeDrivers = new FreeDriversCatalog(driverRepository.findAllFreeDrivers());
        Specification<Driver> specification = rideSpecificationFactory.create(requestRideCommand);

        return offerRideWith(freeDrivers.selectSuitable(specification));
    }

    private RideOffer offerRideWith(Driver driver) {
        return new RideOffer(driver.getId());
    }
}
