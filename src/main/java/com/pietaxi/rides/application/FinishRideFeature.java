package com.pietaxi.rides.application;

import com.pietaxi.rides.domain.model.HistoricalRidesRepository;
import com.pietaxi.rides.domain.model.Location;
import com.pietaxi.rides.domain.model.Ride;
import com.pietaxi.rides.domain.model.RideSnapshot;
import com.pietaxi.rides.domain.model.RidesRepository;

/**
 * @author pawel szymczyk
 */

public class FinishRideFeature {

    private final RidesRepository ridesRepository;
    private final HistoricalRidesRepository historicalRidesRepository;

    public FinishRideFeature(RidesRepository ridesRepository,
                             HistoricalRidesRepository historicalRidesRepository) {
        this.ridesRepository = ridesRepository;
        this.historicalRidesRepository = historicalRidesRepository;
    }

    public void finishRide(FinishRideCommand finishRideCommand) {
        Ride ride = ridesRepository.findById(finishRideCommand.getRideId());
        RideSnapshot rideSnapshot = ride.finish(new Location(finishRideCommand.getTargetLatitude(), finishRideCommand.getTargetLongitude()));
        historicalRidesRepository.save(rideSnapshot);
        ridesRepository.delete(ride);
    }
}
