package com.pietaxi.rides.application;

import com.pietaxi.sharedkernel.DriverId;
import com.pietaxi.sharedkernel.PassengerId;

/**
 * @author pawel szymczyk
 */
public class StartRideCommand {

    private PassengerId passengerId;
    private DriverId driverId;
    private long pickLongitude;
    private long pickLatitude;

    public StartRideCommand(PassengerId passengerId, DriverId driverId, long pickLongitude, long pickLatitude) {
        this.passengerId = passengerId;
        this.driverId = driverId;
        this.pickLongitude = pickLongitude;
        this.pickLatitude = pickLatitude;
    }

    public PassengerId getPassengerId() {
        return passengerId;
    }

    public DriverId getDriverId() {
        return driverId;
    }

    public long getPickLongitude() {
        return pickLongitude;
    }

    public long getPickLatitude() {
        return pickLatitude;
    }
}
