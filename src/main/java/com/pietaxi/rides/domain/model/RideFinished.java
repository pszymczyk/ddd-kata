package com.pietaxi.rides.domain.model;

import java.time.LocalDateTime;
import java.util.UUID;

import com.pietaxi.common.ddd.event.DomainEvent;
import com.pietaxi.sharedkernel.RideId;

/**
 * @author pawel szymczyk
 */
public class RideFinished implements DomainEvent {

    private final UUID id;
    private final LocalDateTime occurrenceTime;
    private final RideId rideId;

    public RideFinished(LocalDateTime occurrenceTime, RideId rideId) {
        this.id = UUID.randomUUID();
        this.occurrenceTime = occurrenceTime;
        this.rideId = rideId;
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public LocalDateTime getOccurrenceTime() {
        return occurrenceTime;
    }

    public RideId getRideId() {
        return rideId;
    }
}