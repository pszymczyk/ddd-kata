package com.pietaxi.rides.domain.model;

import com.pietaxi.sharedkernel.RideId;

/**
 * @author pawel szymczyk
 */
public interface HistoricalRidesRepository {

    void save(RideSnapshot rideSnapshot);

    RideSnapshot findById(RideId rideId);
}
