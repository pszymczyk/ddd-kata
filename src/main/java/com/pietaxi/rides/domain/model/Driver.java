package com.pietaxi.rides.domain.model;

import java.util.ArrayList;
import java.util.List;

import com.pietaxi.common.ddd.Entity;
import com.pietaxi.sharedkernel.DriverId;

/**
 * @author pawel szymczyk
 */
public class Driver extends Entity<DriverId> {

    private final List<DriverFeature> features;

    public Driver(DriverId driverId) {
        this(driverId, new ArrayList<>());
    }

    private Driver(DriverId driverId, List<DriverFeature> features) {
        super(driverId);
        this.features = features;
    }

    public DriverId getId() {
        return id;
    }

    boolean isBusinessLine() {
        return features.stream().anyMatch(feature -> feature.hasFeature(BusinessLineFeature.NAME));
    }

    boolean hasElectricCar() {
        return features.stream().anyMatch(feature -> feature.hasFeature(ElectricCarFeature.NAME));
    }

    boolean hasAdditionalFeatures() {
        return isBusinessLine() || hasElectricCar();
    }

    public Driver registerFeature(DriverFeature driverFeature) {
        features.add(driverFeature);
        return this;
    }
}
