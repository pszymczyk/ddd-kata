package com.pietaxi.rides.domain.model;

import com.pietaxi.sharedkernel.RideId;

/**
 * @author pawel szymczyk
 */
public interface RidesRepository {

    Ride findById(RideId rideId);

    void save(Ride ride);

    void delete(Ride ride);
}
