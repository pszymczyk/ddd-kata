package com.pietaxi.rides.domain.model;

import com.pietaxi.sharedkernel.RideId;

/**
 * @author pawel szymczyk
 */
class CannotFinishAlreadyFinishedRide extends RuntimeException {
    CannotFinishAlreadyFinishedRide(RideId rideId) {
        super("Cannot finish already finished ride, id: " + rideId);
    }
}
