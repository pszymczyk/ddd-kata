package com.pietaxi.rides.domain.model;

import java.time.Clock;
import java.time.LocalDateTime;

import com.pietaxi.common.ddd.DomainTools;
import com.pietaxi.common.ddd.Entity;
import com.pietaxi.common.ddd.event.EventPublisher;
import com.pietaxi.common.ddd.vo.Period;
import com.pietaxi.sharedkernel.DriverId;
import com.pietaxi.sharedkernel.PassengerId;
import com.pietaxi.sharedkernel.RideId;

/**
 * @author pawel szymczyk
 */
public class Ride extends Entity<RideId> {

    public enum Status {
        PRESENT,
        FINISHED
    }

    private final DriverId driverId;
    private final PassengerId passengerId;
    private final Location startLocation;
    private final LocalDateTime startTime;

    private final EventPublisher eventPublisher;
    private final Clock clock;

    private Status status = Status.PRESENT;

    public Ride(
            RideId rideId,
            DriverId driverId,
            PassengerId passengerId,
            Location startLocation,
            LocalDateTime startTime,
            DomainTools domainTools) {
        super(rideId);
        this.driverId = driverId;
        this.passengerId = passengerId;
        this.startLocation = startLocation;
        this.startTime = startTime;
        this.eventPublisher = domainTools.getEventPublisher();
        this.clock = domainTools.getClock();
    }

    public RideSnapshot finish(Location finishLocation) {
        if (isFinished()) {
            throw new CannotFinishAlreadyFinishedRide(id);
        }

        markAsFinished();

        LocalDateTime endTime = LocalDateTime.now(clock);
        Period period = new Period(startTime, endTime);
        Route route = new Route(startLocation, finishLocation);

        eventPublisher.publishEvent(new RideFinished(endTime, id));

        return new RideSnapshot(id, driverId, passengerId, period, route);
    }

    boolean isFinished() {
        return status == Status.FINISHED;
    }

    private void markAsFinished() {
        status = Status.FINISHED;
    }

    public DriverId getDriverId() {
        return driverId;
    }

    public PassengerId getPassengerId() {
        return passengerId;
    }

    public Location getStartLocation() {
        return startLocation;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public Status getStatus() {
        return status;
    }
}
