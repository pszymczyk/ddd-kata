package com.pietaxi.rides.domain.model;

import com.pietaxi.common.ddd.specification.ConjunctionSpecification;
import com.pietaxi.common.ddd.specification.Specification;
import com.pietaxi.rides.application.RequestRideCommand;

/**
 * @author pawel szymczyk
 */
public class RideSpecificationFactory {

    public Specification<Driver> create(RequestRideCommand requestRideCommand) {
        Specification conjunctionSpecification = new ConjunctionSpecification();

        if (requestRideCommand.isBusinessLineRequired()) {
            conjunctionSpecification = conjunctionSpecification.and(new BusinessLineSpecification());
        }

        if (requestRideCommand.isElectricCarRequired()) {
            conjunctionSpecification = conjunctionSpecification.and(new ElectricCarSpecification());
        }

        return conjunctionSpecification;
    }
}
