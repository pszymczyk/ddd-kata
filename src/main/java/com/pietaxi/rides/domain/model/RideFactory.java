package com.pietaxi.rides.domain.model;

import java.time.LocalDateTime;

import com.pietaxi.common.ddd.DomainTools;
import com.pietaxi.crm.CrmFacade;
import com.pietaxi.sharedkernel.DriverId;
import com.pietaxi.sharedkernel.PassengerId;
import com.pietaxi.sharedkernel.RideId;

/**
 * @author pawel szymczyk
 */
public class RideFactory {

    private final CrmFacade crmFacade;
    private final DriverRepository driverRepository;
    private final DomainTools domainTools;

    public RideFactory(
            CrmFacade crmFacade,
            DriverRepository driverRepository,
            DomainTools domainTools) {
        this.crmFacade = crmFacade;
        this.driverRepository = driverRepository;
        this.domainTools = domainTools;
    }

    public Ride create(PassengerId passengerId, DriverId driverId, long pickLatitude, long pickLongitude) {
        LocalDateTime startTime = LocalDateTime.now(domainTools.getClock());

        Driver driver = driverRepository.findById(driverId);
        if (!crmFacade.isUserActive(passengerId)) {
            throw new UserNotActiveException(passengerId);
        }

        Location location = new Location(pickLatitude, pickLongitude);

        return new Ride(
                RideId.random(),
                driver.getId(),
                passengerId,
                location,
                startTime,
                domainTools);
    }
}
