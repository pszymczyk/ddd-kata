package com.pietaxi.rides.domain.model;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * @author pawel szymczyk
 */
public final class Route {
    private final Location startLocation;
    private final Location targetLocation;

    public Route(Location startLocation, Location targetLocation) {
        this.startLocation = startLocation;
        this.targetLocation = targetLocation;
    }

    /**
     * @return simple distance between two points calculated by right angled triangle formula
     */
    long getDistanceInKm() {
        long x1 = startLocation.getLatitude();
        long y1 = startLocation.getLongitude();
        long x2 = targetLocation.getLatitude();
        long y2 = startLocation.getLongitude();

        double distance = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));

        return new BigDecimal(distance).setScale(0, BigDecimal.ROUND_HALF_UP).longValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Route route = (Route) o;
        return Objects.equals(startLocation, route.startLocation) &&
                Objects.equals(targetLocation, route.targetLocation);
    }

    public Location getStartLocation() {
        return startLocation;
    }

    public Location getTargetLocation() {
        return targetLocation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(startLocation, targetLocation);
    }
}
