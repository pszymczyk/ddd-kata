package com.pietaxi.rides.domain.model;

import com.pietaxi.common.ddd.vo.Period;
import com.pietaxi.sharedkernel.DriverId;
import com.pietaxi.sharedkernel.PassengerId;
import com.pietaxi.sharedkernel.RideId;

/**
 * @author pawel szymczyk
 */
public class RideSnapshot {

    private final RideId rideId;
    private final DriverId driverId;
    private final PassengerId passengerId;
    private final Period period;
    private final Route route;

    public RideSnapshot(RideId rideId, DriverId driverId, PassengerId passengerId, Period period, Route route) {
        this.rideId = rideId;
        this.driverId = driverId;
        this.passengerId = passengerId;
        this.period = period;
        this.route = route;
    }

    public RideId getRideId() {
        return rideId;
    }

    public PassengerId getPassengerId() {
        return passengerId;
    }

    public DriverId getDriverId() {
        return driverId;
    }

    public Period getPeriod() {
        return period;
    }

    public Route getRoute() {
        return route;
    }

    public long distanceInKm() {
        return route.getDistanceInKm();
    }
}
