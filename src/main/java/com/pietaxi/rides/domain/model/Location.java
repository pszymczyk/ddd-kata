package com.pietaxi.rides.domain.model;

import java.util.Objects;

/**
 * @author pawel szymczyk
 */
public final class Location {

    private final long latitude;
    private final long longitude;

    public Location(long latitude, long longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    long getLatitude() {
        return latitude;
    }

    long getLongitude() {
        return longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return latitude == location.latitude &&
                longitude == location.longitude;
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }

    public String asString() {
        return latitude+","+longitude;
    }
}
