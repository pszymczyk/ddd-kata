package com.pietaxi.rides.domain.model;

import com.pietaxi.sharedkernel.DriverId;

/**
 * @author pawel szymczyk
 */
public class RideOffer {
    private final DriverId driverId;

    public RideOffer(DriverId driverId) {
        this.driverId = driverId;
    }

    public DriverId getDriverId() {
        return driverId;
    }
}
