package com.pietaxi.rides.domain.model;

interface DriverFeature {

    default boolean hasFeature(String feature) {
        return false;
    }

}
