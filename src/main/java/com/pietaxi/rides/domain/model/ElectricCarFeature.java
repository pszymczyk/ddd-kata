package com.pietaxi.rides.domain.model;

/**
 * @author pawel szymczyk
 */
class ElectricCarFeature implements DriverFeature {

    static String NAME = "electric-car";

    @Override
    public boolean hasFeature(String feature) {
        return NAME.equalsIgnoreCase(feature);
    }
}
