package com.pietaxi.rides.domain.model;

import com.pietaxi.sharedkernel.PassengerId;

/**
 * @author pawel szymczyk
 */
class UserNotActiveException extends RuntimeException {
    UserNotActiveException(PassengerId id) {
        super("Account " + id + " is suspended! cannot invoke requested action.");
    }
}
