package com.pietaxi.rides.domain.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.pietaxi.common.ddd.specification.Specification;
import com.pietaxi.rides.application.CannotFindSuitableDriver;

/**
 * @author pawel szymczyk
 */
public class FreeDriversCatalog {

    private final List<Driver> drivers;

    public FreeDriversCatalog(List<Driver> drivers) {
        this.drivers = Collections.unmodifiableList(drivers);
    }


    public Driver selectSuitable(Specification<Driver> specification) {
        List<Driver> availableDrivers = new ArrayList<>(drivers);

        preferRegularDrivers(availableDrivers);

        for (Driver driver: availableDrivers) {
            if (specification.isSatisfiedBy(driver)) {
                return driver;
            }
        }

        throw new CannotFindSuitableDriver();
    }

    private void preferRegularDrivers(List<Driver> freeDrivers) {
        Collections.sort(freeDrivers, (o1, o2) -> Boolean.compare(o1.hasAdditionalFeatures(), o2.hasAdditionalFeatures()));
    }
}
