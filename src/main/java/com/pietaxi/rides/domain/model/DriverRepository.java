package com.pietaxi.rides.domain.model;

import java.util.List;

import com.pietaxi.sharedkernel.DriverId;

/**
 * @author pawel szymczyk
 */
public interface DriverRepository {

    void save(Driver driver);

    Driver findById(DriverId driverId);

    List<Driver> findAllFreeDrivers();
}
