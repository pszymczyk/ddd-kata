package com.pietaxi.rides.domain.model;

import com.pietaxi.common.ddd.specification.CompositeSpecification;

/**
 * @author pawel szymczyk
 */
public class ElectricCarSpecification extends CompositeSpecification<Driver> {
    @Override
    public boolean isSatisfiedBy(Driver candidate) {
        return candidate.hasElectricCar();
    }
}
