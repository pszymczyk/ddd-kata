package com.pietaxi.rides.domain.model;

class BusinessLineFeature implements DriverFeature {

    static String NAME = "business-line";

    @Override
    public boolean hasFeature(String feature) {
        return NAME.equalsIgnoreCase(feature);
    }
}
