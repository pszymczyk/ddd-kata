package com.pietaxi.rides;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pietaxi.rides.domain.model.HistoricalRidesRepository;

@Configuration
public class RidesFacadeConfiguration {

    @Autowired
    HistoricalRidesRepository historicalRidesRepository;

    @Bean
    RidesFacade ridesFacade() {
        return new RidesFacade(historicalRidesRepository);
    }
}
