package com.pietaxi.rides;

import java.util.Optional;

import com.pietaxi.rides.domain.model.HistoricalRidesRepository;
import com.pietaxi.sharedkernel.RideId;

public class RidesFacade {

    private HistoricalRidesRepository historicalRidesRepository;

    public RidesFacade(HistoricalRidesRepository historicalRidesRepository) {
        this.historicalRidesRepository = historicalRidesRepository;
    }

    public Optional<RideDto> getRideDetails(RideId rideId) {
        return Optional.ofNullable(historicalRidesRepository.findById(rideId))
            .map(snapshot -> new RideDto(snapshot));
    }
}
