package com.pietaxi.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;

import com.pietaxi.common.CommonConfiguration;
import com.pietaxi.crm.CrmFacadeConfiguration;
import com.pietaxi.crm.application.CrmApiConfiguration;
import com.pietaxi.crm.port.adapter.CrmAdaptersConfiguration;
import com.pietaxi.finance.port.adapter.FinanceAdaptersConfiguration;
import com.pietaxi.rides.RidesFacadeConfiguration;
import com.pietaxi.rides.application.RidesApplicationConfiguration;
import com.pietaxi.rides.port.adapter.rest.RidesRestAdapterConfiguration;
import com.pietaxi.rides.port.adapter.sql.RidesSqlAdapterConfiguration;

/**
 * @author pawel szymczyk
 */
@EnableAutoConfiguration
@ImportAutoConfiguration(classes = {
        CrmApiConfiguration.class,
        CrmAdaptersConfiguration.class,
        CrmFacadeConfiguration.class,
        FinanceAdaptersConfiguration.class,
        RidesApplicationConfiguration.class,
        RidesSqlAdapterConfiguration.class,
        RidesRestAdapterConfiguration.class,
        RidesFacadeConfiguration.class,
        CommonConfiguration.class
})
public class PietaxiApp {

    public static void main(String[] args) {
        SpringApplication.run(PietaxiApp.class, args);
    }
}
