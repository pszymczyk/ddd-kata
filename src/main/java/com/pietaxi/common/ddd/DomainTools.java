package com.pietaxi.common.ddd;

import java.time.Clock;

import com.pietaxi.common.ddd.event.EventPublisher;

public class DomainTools {

    private final EventPublisher eventPublisher;
    private final Clock clock;

    public DomainTools(EventPublisher eventPublisher, Clock clock) {
        this.eventPublisher = eventPublisher;
        this.clock = clock;
    }

    public EventPublisher getEventPublisher() {
        return eventPublisher;
    }

    public Clock getClock() {
        return clock;
    }
}
