package com.pietaxi.common.ddd.event;

/**
 * @author pawel szymczyk
 */
public interface EventPublisher {

    void publishEvent(DomainEvent domainEvent);
}
