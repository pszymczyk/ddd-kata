package com.pietaxi.common.ddd.event;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author pawel szymczyk
 */
public interface DomainEvent {

    UUID getId();

    LocalDateTime getOccurrenceTime();
}
