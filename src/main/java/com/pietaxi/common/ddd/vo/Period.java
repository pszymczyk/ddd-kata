package com.pietaxi.common.ddd.vo;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;


/**
 * @author pawel szymczyk
 */
public class Period {
    private final LocalDateTime startTime;
    private final LocalDateTime endTime;

    public Period(LocalDateTime startTime, LocalDateTime endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public long periodInMinutes() {
        return ChronoUnit.MINUTES.between(startTime, endTime);
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Period period = (Period) o;
        return Objects.equals(startTime, period.startTime) &&
                Objects.equals(endTime, period.endTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startTime, endTime);
    }
}
