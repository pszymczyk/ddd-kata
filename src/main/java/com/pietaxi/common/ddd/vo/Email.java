package com.pietaxi.common.ddd.vo;

import java.util.Objects;

/**
 * @author pawel szymczyk
 */
public class Email {

    private String domain;
    private String name;

    public Email(String name, String domain) {
        this.domain = name;
        this.name = domain;
    }

    public static Email parse(String email) {
        String[] split = email.split("@");
        return new Email(split[0], split[1]);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Email email = (Email) o;
        return Objects.equals(domain, email.domain) &&
                Objects.equals(name, email.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(domain, name);
    }
}
