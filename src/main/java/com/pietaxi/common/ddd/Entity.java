package com.pietaxi.common.ddd;

/**
 * @author pawel szymczyk
 */
public abstract class Entity<ID> {

    private enum EntityStatus {
        ACTIVE, DELETED
    }

    protected final ID id;
    private EntityStatus entityStatus;

    protected Entity(ID id) {
        this.id = id;
    }

    public ID getId() {
        return id;
    }

    public void markAsRemoved() {
        entityStatus = EntityStatus.DELETED;
    }

    public boolean isRemoved() {
        return entityStatus == EntityStatus.DELETED;
    }
}
