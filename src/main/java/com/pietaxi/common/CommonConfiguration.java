package com.pietaxi.common;

import java.time.Clock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import com.pietaxi.common.ddd.DomainTools;
import com.pietaxi.common.ddd.event.EventPublisher;

/**
 * @author pawel szymczyk
 */
@Configuration
@EnableAsync
public class CommonConfiguration {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Bean
    DomainTools domainTools(EventPublisher eventPublisher, Clock clock) {
        return new DomainTools(eventPublisher, clock);
    }

    @Bean
    @ConditionalOnMissingBean
    public EventPublisher eventPublisher() {
        return domainEvent -> applicationEventPublisher.publishEvent(domainEvent);
    }

    @Bean
    @ConditionalOnMissingBean
    public Clock clock() {
        return Clock.systemUTC();
    }
}
