package com.pietaxi.crm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pietaxi.crm.domain.model.UserRepository;

@Configuration
public class CrmFacadeConfiguration {

    @Autowired
    UserRepository userRepository;

    @Bean
    public CrmFacade crmFacade() {
        return new CrmFacade(userRepository);
    }
}
