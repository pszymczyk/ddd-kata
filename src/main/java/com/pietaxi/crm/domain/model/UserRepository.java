package com.pietaxi.crm.domain.model;

import java.util.Optional;

import com.pietaxi.sharedkernel.PassengerId;

public interface UserRepository {

    void save(User user);

    boolean exists(UserId userId);

    Optional<User> findByPassengerId(PassengerId passengerId);
}
