package com.pietaxi.crm.domain.model;

import static com.pietaxi.crm.domain.model.SurveyLine.RateScale.ONE_TO_NINE;

public class SurveyFactory {

    public Survey defaultSurvey() {
        Survey survey = new Survey(
                new SurveyLine("How do you rate the ride?", ONE_TO_NINE),
                new SurveyLine("How do you rate your fellow traveler?", ONE_TO_NINE)
        );

        return survey;
    }

}
