package com.pietaxi.crm.domain.model;

import com.pietaxi.common.ddd.Entity;
import com.pietaxi.common.ddd.vo.Email;
import com.pietaxi.sharedkernel.PassengerId;

/**
 * @author pawel szymczyk
 */
public class User extends Entity<UserId> {

    private final PassengerId passengerId;
    private final String name;
    private final Email email;
    private final Address address;

    public User(UserId id, PassengerId passengerId, String name, Email email, Address address) {
        super(id);
        this.passengerId = passengerId;
        this.name = name;
        this.email = email;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public Email getEmail() {
        return email;
    }

    Address getAddress() {
        return address;
    }

    public PassengerId getPassengerId() {
        return passengerId;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", email=" + email +
                '}';
    }
}
