package com.pietaxi.crm.domain.model;

/**
 * @author pawel szymczyk
 */
public interface SurveySender {

    void send(Survey survey, User user);
}
