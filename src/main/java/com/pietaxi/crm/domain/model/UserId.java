package com.pietaxi.crm.domain.model;

import java.util.Objects;

public class UserId {

    private final String id;

    public UserId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserId userId1 = (UserId) o;
        return Objects.equals(id, userId1.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    public String asString() {
        return id;
    }
}
