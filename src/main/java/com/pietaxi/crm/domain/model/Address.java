package com.pietaxi.crm.domain.model;

import java.util.Arrays;
import java.util.List;

public class Address {

    private final List<String> lines;

    public static Address fromString(String address) {
        String[] splitLines = address.split("\n");

        if (splitLines.length > 3) {
            throw new InvalidAddressFormat(address);
        }

        return new Address(splitLines);
    }

    private Address(String[] lines) {
        this.lines = Arrays.asList(lines);
    }

    @Override
    public String toString() {
        return lines.stream().reduce("", (left, right) -> left + "\n" + right);
    }
}
