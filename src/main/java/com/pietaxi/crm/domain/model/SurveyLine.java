package com.pietaxi.crm.domain.model;

class SurveyLine {

    public enum RateScale {
        ONE_TO_NINE
    }

    private final String question;
    private final RateScale rateScale;

    static SurveyLine valueOf(String string) {
        int separatorIndex = string.indexOf(" ${");
        String question = cutQuestion(string, separatorIndex);
        RateScale rateScale = RateScale.valueOf(cutRateScale(string, separatorIndex));
        return new SurveyLine(question, rateScale);
    }

    private static String cutQuestion(String string, int separatorIndex) {
        return string.substring(0, separatorIndex);
    }

    private static String cutRateScale(String string, int separatorIndex) {
        return string.substring(separatorIndex+3, string.length()-1);
    }

    SurveyLine(String question, RateScale rateScale) {
        this.question = question;
        this.rateScale = rateScale;
    }

    String asString() {
        return question + " ${" + rateScale + "}\n";
    }

}
