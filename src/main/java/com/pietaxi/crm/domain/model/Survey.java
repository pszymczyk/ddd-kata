package com.pietaxi.crm.domain.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @author pawel szymczyk
 */
public class Survey {

    private final List<SurveyLine> lines;

    static Survey valueOf(String string) {
        List<SurveyLine> lines = Arrays.stream(string.split("\n"))
              .map(SurveyLine::valueOf)
              .collect(toList());

        return new Survey(lines);

    }

    Survey(SurveyLine... lines) {
        this(Arrays.asList(lines));
    }

    Survey(List<SurveyLine> lines) {
        this.lines = Collections.unmodifiableList(lines);
    }

    public String asString() {
        return lines.stream()
                .map(SurveyLine::asString)
                .reduce(String::concat)
                .orElseThrow(() -> new IllegalStateException("Survey should has at least one question"));
    }
}
