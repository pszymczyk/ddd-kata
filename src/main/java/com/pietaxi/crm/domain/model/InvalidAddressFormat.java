package com.pietaxi.crm.domain.model;

class InvalidAddressFormat extends RuntimeException {

    InvalidAddressFormat(String address) {
        super("Could not parse given address: " + address + ". Address should contains at most 3 lines");
    }
}
