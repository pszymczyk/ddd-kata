package com.pietaxi.crm.port.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.pietaxi.crm.domain.model.UserRepository;
import com.pietaxi.sharedkernel.PassengerId;

/**
 * @author pawel szymczyk
 */
@RestController
class CrmRestAdapter {

    @Autowired
    UserRepository userRepository;

    @GetMapping(path = "/users")
    public void finishRide(@RequestParam String passengerId) {
        userRepository.findByPassengerId(PassengerId.from(passengerId))
            .orElseThrow(() -> new UserNotExistsException(passengerId));
    }


    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    static class UserNotExistsException extends RuntimeException {

        public UserNotExistsException(String passengerId) {
            super("User with passenger id " + passengerId + "does not exists");
        }
    }
}
