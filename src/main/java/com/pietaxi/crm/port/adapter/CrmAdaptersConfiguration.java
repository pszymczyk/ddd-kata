package com.pietaxi.crm.port.adapter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pietaxi.crm.application.SendSurveyFeature;
import com.pietaxi.crm.domain.model.SurveyFactory;
import com.pietaxi.crm.domain.model.SurveySender;
import com.pietaxi.crm.domain.model.UserRepository;
import com.pietaxi.rides.RidesFacade;

/**
 * @author pawel szymczyk
 */
@Configuration
public class CrmAdaptersConfiguration {

    @Bean
    RideDomainEventsListener rideDomainEventsListener(
            SurveySender surveySender,
            UserRepository userRepository,
            RidesFacade ridesFacade) {
        SendSurveyFeature sendSurveyFeature = new SendSurveyFeature(surveySender, userRepository, new SurveyFactory());
        return new RideDomainEventsListener(sendSurveyFeature, ridesFacade);
    }

    @Bean
    public UserRepository userRepository() {
        return new InMemoryUserRepository();
    }

    @Bean
    public CrmRestAdapter crmRestAdapter() {
        return new CrmRestAdapter();
    }

    @Bean
    public SurveySender loggingSurveySender() {
        return new LoggingSurveySender();
    }
}
