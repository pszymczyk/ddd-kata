package com.pietaxi.crm.port.adapter;

import java.util.function.Function;

import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;

import com.pietaxi.crm.application.SendSurveyCommand;
import com.pietaxi.crm.application.SendSurveyFeature;
import com.pietaxi.rides.RidesFacade;
import com.pietaxi.rides.domain.model.RideFinished;
import com.pietaxi.sharedkernel.PassengerId;

import io.github.resilience4j.retry.Retry;

class RideDomainEventsListener {

    private final SendSurveyFeature sendSurveyFeature;
    private final RidesFacade ridesFacade;

    public RideDomainEventsListener(SendSurveyFeature sendSurveyFeature, RidesFacade ridesFacade) {
        this.sendSurveyFeature = sendSurveyFeature;
        this.ridesFacade = ridesFacade;
    }

    @Async
    @EventListener
    public void onRideFinished(RideFinished rideFinished) {
        sendSurveyFeature.sendSurvey(new SendSurveyCommand(getPassengerId().apply(rideFinished)));
    }

    private Function<RideFinished, PassengerId> getPassengerId() {
        return Retry.decorateFunction(Retry.ofDefaults("RetryGetRideDetails"), (r) -> ridesFacade.getRideDetails(r.getRideId()).get().getPassengerId());
    }
}
