package com.pietaxi.crm.port.adapter;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pietaxi.common.ddd.vo.Email;
import com.pietaxi.crm.domain.model.Address;
import com.pietaxi.crm.domain.model.User;
import com.pietaxi.crm.domain.model.UserId;
import com.pietaxi.crm.domain.model.UserRepository;
import com.pietaxi.sharedkernel.PassengerId;

class InMemoryUserRepository implements UserRepository {

    private static final Logger log = LoggerFactory.getLogger(InMemoryUserRepository.class);
    private static final Map<UserId, User> storage = new HashMap<>();

    static {
        IntStream.range(0, 10)
                 .forEach(i -> {
                     User user = new User(new UserId(i+""), PassengerId.random(), "user"+i, Email.parse(i+"@gmail.com"), Address.fromString("Jana Nowaka-Jezioranskiego 47/"+i));
                     storage.put(user.getId(), user);
                     log.info("Adding test user to database, passengerId " + user.getPassengerId());
                 });
    }

    @Override
    public void save(User user) {
        storage.put(user.getId(), user);
    }

    @Override
    public boolean exists(UserId userId) {
        return storage.containsKey(userId);
    }

    @Override
    public Optional<User> findByPassengerId(PassengerId passengerId) {
        return storage.values()
               .stream()
               .filter(u -> u.getPassengerId().equals(passengerId))
               .findFirst();
    }
}

