package com.pietaxi.crm.port.adapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pietaxi.crm.domain.model.Survey;
import com.pietaxi.crm.domain.model.SurveySender;
import com.pietaxi.crm.domain.model.User;

class LoggingSurveySender implements SurveySender {

    private static final Logger log = LoggerFactory.getLogger(LoggingSurveySender.class);

    @Override
    public void send(Survey survey, User user) {
        log.info("Sending survey to user: " + user.getEmail());
    }
}
