package com.pietaxi.crm.application;

/**
 * @author pawel szymczyk
 */
class UserAlreadyExists extends RuntimeException {

    UserAlreadyExists(String username) {
        super("User with name " + username + " already exists");
    }
}
