package com.pietaxi.crm.application;

import com.pietaxi.crm.domain.model.SurveyFactory;
import com.pietaxi.crm.domain.model.SurveySender;
import com.pietaxi.crm.domain.model.User;
import com.pietaxi.crm.domain.model.UserRepository;

/**
 * @author pawel szymczyk
 */
public class SendSurveyFeature {

    private final SurveySender surveySender;
    private final UserRepository userRepository;
    private final SurveyFactory surveyFactory;

    public SendSurveyFeature(SurveySender surveySender, UserRepository userRepository, SurveyFactory surveyFactory) {
        this.surveySender = surveySender;
        this.userRepository = userRepository;
        this.surveyFactory = surveyFactory;
    }

    public void sendSurvey(SendSurveyCommand command) {
        User user = userRepository.findByPassengerId(command.getPassengerId()).orElseThrow(() -> new IllegalStateException("Cannot send survey to unknown user!"));

        surveySender.send(surveyFactory.defaultSurvey(), user);
    }
}
