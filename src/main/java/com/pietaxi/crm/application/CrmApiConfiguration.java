package com.pietaxi.crm.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pietaxi.crm.domain.model.SurveyFactory;
import com.pietaxi.crm.domain.model.SurveySender;
import com.pietaxi.crm.domain.model.UserRepository;

/**
 * @author pawel szymczyk
 */
@Configuration
public class CrmApiConfiguration {

    @Autowired
    private SurveySender surveySender;

    @Autowired
    private UserRepository userRepository;

    @Bean
    public SendSurveyFeature sendingSurveyFeature() {
        SurveyFactory surveyFactory = new SurveyFactory();
        return new SendSurveyFeature(surveySender, userRepository, surveyFactory);
    }
}
