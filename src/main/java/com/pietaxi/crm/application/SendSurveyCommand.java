package com.pietaxi.crm.application;

import com.pietaxi.sharedkernel.PassengerId;

public class SendSurveyCommand {

    private final PassengerId passengerId;

    public SendSurveyCommand(PassengerId passengerId) {
        this.passengerId = passengerId;
    }

    public PassengerId getPassengerId() {
        return passengerId;
    }
}
