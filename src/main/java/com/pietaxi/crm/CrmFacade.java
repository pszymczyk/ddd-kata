package com.pietaxi.crm;

import com.pietaxi.crm.domain.model.UserRepository;
import com.pietaxi.sharedkernel.PassengerId;

public class CrmFacade {

    private final UserRepository userRepository;

    public CrmFacade(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean isUserActive(PassengerId passengerId) {
        return userRepository.findByPassengerId(passengerId).isPresent();
    }
}
