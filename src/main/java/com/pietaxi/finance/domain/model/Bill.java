package com.pietaxi.finance.domain.model;

import com.pietaxi.common.ddd.Entity;
import com.pietaxi.common.ddd.vo.Money;
import com.pietaxi.common.ddd.vo.Period;
import com.pietaxi.sharedkernel.PassengerId;

/**
 * @author pawel szymczyk
 */
public class Bill extends Entity<BillId> {

    private final PassengerId passengerId;
    private final BillingStrategy billingStrategy;
    private final Period period;
    private final long distanceInKm;

    Bill(BillId billId, PassengerId passengerId, BillingStrategy billingStrategy, Period period, long distanceInKm) {
        super(billId);
        this.passengerId = passengerId;
        this.billingStrategy = billingStrategy;
        this.period = period;
        this.distanceInKm = distanceInKm;
    }

    public Money account() {
        return billingStrategy.calculate(new RideSummary(distanceInKm, period));
    }

    public PassengerId getPassengerId() {
        return passengerId;
    }
}
