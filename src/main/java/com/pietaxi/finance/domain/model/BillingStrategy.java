package com.pietaxi.finance.domain.model;

import java.util.Arrays;

import com.pietaxi.common.ddd.vo.Money;

import static com.pietaxi.common.ddd.vo.Money.zero;

public interface BillingStrategy {

    String getName();

    Money calculate(RideSummary rideSummary);

    static BillingStrategy defaultStrategy() {
        return new BillingStrategy() {
            @Override
            public String getName() {
                return "default";
            }

            @Override
            public Money calculate(RideSummary rideSummary) {
                return zero().add(Money.of("1").times(rideSummary.getDistanceInKm()))
                             .add(Money.of("1").times(rideSummary.getPeriod().periodInMinutes())
                                       .add(Money.of("5")));
            }
        };
    }

    static BillingStrategy feesChain(Fee... fee) {
        return new BillingStrategy() {
            @Override
            public String getName() {
                return "FeesChain";
            }

            @Override
            public Money calculate(RideSummary rideSummary) {
                return Arrays.stream(fee)
                             .map(e -> e.calculate(rideSummary))
                             .reduce(zero(), Money::add);
            }
        };
    }
}
