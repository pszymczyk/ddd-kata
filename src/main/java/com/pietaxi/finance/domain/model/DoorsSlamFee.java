package com.pietaxi.finance.domain.model;

import com.pietaxi.common.ddd.vo.Money;

/**
 * @author pawel szymczyk
 */
class DoorsSlamFee implements Fee {

    private final Money doorsSlamFee;

    public DoorsSlamFee(Money doorsSlamFee) {
        this.doorsSlamFee = doorsSlamFee;
    }

    public Money calculate(RideSummary rideSummary) {
        return doorsSlamFee;
    }

    @Override
    public String getName() {
        return "Doors slam";
    }
}
