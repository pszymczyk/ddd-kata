package com.pietaxi.finance.domain.model;

import com.pietaxi.common.ddd.vo.Money;
import com.pietaxi.sharedkernel.PassengerId;

/**
 * @author pawel szymczyk
 */
public interface ERPClient {
    void send(PassengerId passengerId, Money fee);
}
