package com.pietaxi.finance.domain.model;

import java.util.Objects;
import java.util.UUID;

/**
 * @author pawel szymczyk
 */
public class BillId {

    private final UUID id;

    public static BillId random() {
        return new BillId(UUID.randomUUID());
    }

    public static BillId from(String id) {
        return new BillId(UUID.fromString(id));
    }

    private BillId(UUID id) {
        this.id = id;
    }

    public String asString() {
        return id.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BillId billId = (BillId) o;
        return id.equals(billId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
