package com.pietaxi.finance.domain.model;

import com.pietaxi.rides.RideDto;
import com.pietaxi.rides.RidesFacade;
import com.pietaxi.sharedkernel.RideId;

/**
 * @author pawel szymczyk
 */
public class BillFactory {

    private final RidesFacade ridesFacade;
    private final DriverRepository driverRepository;
    private final BillingStrategyRepository billingStrategyRepository;

    public BillFactory(
            RidesFacade ridesFacade,
            DriverRepository driverRepository,
            BillingStrategyRepository billingStrategyRepository) {
        this.ridesFacade = ridesFacade;
        this.driverRepository = driverRepository;
        this.billingStrategyRepository = billingStrategyRepository;
    }

    public Bill create(RideId rideId) {
        if (rideId == null) {
            throw new IllegalArgumentException("RideId cannot be null.");
        }

        RideDto rideDto = ridesFacade.getRideDetails(rideId).get();
        Driver driver = driverRepository.findByDriverId(rideDto.getDriverId());
        BillingStrategy billingStrategy = billingStrategyRepository.findStrategyForDriver(driver).orElse(BillingStrategy.defaultStrategy());

        return new Bill(BillId.random(), rideDto.getPassengerId(), billingStrategy, rideDto.getPeriod(), rideDto.getDistanceInKm());
    }
}
