package com.pietaxi.finance.domain.model;

import com.pietaxi.common.ddd.vo.Money;

/**
 * @author pawel szymczyk
 */
interface Fee {

    Money calculate(RideSummary rideSummary);

    String getName();

}
