package com.pietaxi.finance.domain.model;

import java.util.Objects;

import com.pietaxi.common.ddd.vo.Period;

public final class RideSummary {

    private final long distanceInKm;
    private final Period period;

    RideSummary(long distanceInKm, Period period) {
        this.distanceInKm = distanceInKm;
        this.period = period;
    }

    long getDistanceInKm() {
        return distanceInKm;
    }

    Period getPeriod() {
        return period;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RideSummary that = (RideSummary) o;
        return distanceInKm == that.distanceInKm &&
                period.equals(that.period);
    }

    @Override
    public int hashCode() {
        return Objects.hash(distanceInKm, period);
    }
}
