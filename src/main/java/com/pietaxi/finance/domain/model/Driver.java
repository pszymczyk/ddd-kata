package com.pietaxi.finance.domain.model;

/**
 * @author pawel szymczyk
 */
public class Driver {

    private final String licenceId;
    private final boolean businessLine;

    Driver(String licenceId) {
        this(licenceId, false);
    }

    Driver(String licenceId, boolean businessLine) {
        this.licenceId = licenceId;
        this.businessLine = businessLine;
    }

    public String getLicenceId() {
        return licenceId;
    }

    boolean isBusinessLine() {
        return businessLine;
    }
}
