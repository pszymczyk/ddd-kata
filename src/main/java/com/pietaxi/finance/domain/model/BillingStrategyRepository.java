package com.pietaxi.finance.domain.model;

import java.util.Optional;

public interface BillingStrategyRepository {

    Optional<BillingStrategy> findStrategyForDriver(Driver driver);
}
