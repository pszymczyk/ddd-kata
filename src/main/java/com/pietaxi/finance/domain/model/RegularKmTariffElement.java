package com.pietaxi.finance.domain.model;

import com.pietaxi.common.ddd.vo.Money;

/**
 * @author pawel szymczyk
 */
class RegularKmTariffElement implements Fee {

    private Money perOneKmFee;

    public RegularKmTariffElement(Money perOneKmFee) {
        this.perOneKmFee = perOneKmFee;
    }

    public Money calculate(RideSummary rideSummary) {
        return perOneKmFee.times(rideSummary.getDistanceInKm());
    }

    @Override
    public String getName() {
        return "Per KM fee";
    }
}
