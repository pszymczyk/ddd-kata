package com.pietaxi.finance.domain.model;

import com.pietaxi.common.ddd.vo.Money;

/**
 * @author pawel szymczyk
 */
class BusinessLineFee implements Fee {

    private Money perOneKmFee;

    public BusinessLineFee(Money perOneKmFee) {
        this.perOneKmFee = perOneKmFee;
    }

    public Money calculate(RideSummary rideSummary) {
        return perOneKmFee.times(rideSummary.getDistanceInKm());
    }

    @Override
    public String getName() {
        return "Per KM fee";
    }
}
