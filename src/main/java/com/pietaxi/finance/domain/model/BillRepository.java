package com.pietaxi.finance.domain.model;

/**
 * @author pawel szymczyk
 */
public interface BillRepository {

    void save(Bill bill);
}
