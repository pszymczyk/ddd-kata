package com.pietaxi.finance.domain.model;

import com.pietaxi.common.ddd.vo.Money;

/**
 * @author pawel szymczyk
 */
class RegularTimeTariffElement implements Fee {

    private final Money feePerMinute;

    public RegularTimeTariffElement(Money feePerMinute) {
        this.feePerMinute = feePerMinute;
    }

    public Money calculate(RideSummary rideSummary) {
        return feePerMinute.times(rideSummary.getPeriod().periodInMinutes());
    }

    @Override
    public String getName() {
        return "Ride time";
    }
}
