package com.pietaxi.finance.domain.model;

import com.pietaxi.sharedkernel.DriverId;

/**
 * @author pawel szymczyk
 */
public interface DriverRepository {

    void save(Driver driver);

    Driver findByDriverId(DriverId driverId);
}
