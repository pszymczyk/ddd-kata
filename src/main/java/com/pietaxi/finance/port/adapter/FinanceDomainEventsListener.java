package com.pietaxi.finance.port.adapter;

import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;

import com.pietaxi.finance.application.ChargePassengerFeature;
import com.pietaxi.rides.domain.model.RideFinished;

/**
 * @author pawel szymczyk
 */
class FinanceDomainEventsListener {

    private final ChargePassengerFeature chargePassengerFeature;

    FinanceDomainEventsListener(ChargePassengerFeature chargePassengerFeature) {
        this.chargePassengerFeature = chargePassengerFeature;
    }

    @Async
    @EventListener
    public void onRideFinishedEvent(RideFinished event) {
        chargePassengerFeature.chargePassenger(event.getRideId());
    }
}
