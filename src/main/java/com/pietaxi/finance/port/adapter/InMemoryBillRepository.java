package com.pietaxi.finance.port.adapter;

import java.util.HashMap;
import java.util.Map;

import com.pietaxi.finance.domain.model.Bill;
import com.pietaxi.finance.domain.model.BillId;
import com.pietaxi.finance.domain.model.BillRepository;

class InMemoryBillRepository implements BillRepository {

    private final Map<BillId, Bill> storage = new HashMap<>();

    @Override
    public void save(Bill bill) {
        storage.put(bill.getId(), bill);
    }
}
