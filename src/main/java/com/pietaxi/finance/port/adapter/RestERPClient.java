package com.pietaxi.finance.port.adapter;

import org.springframework.web.client.RestTemplate;

import com.pietaxi.common.ddd.vo.Money;
import com.pietaxi.finance.domain.model.ERPClient;
import com.pietaxi.sharedkernel.PassengerId;

/**
 * @author pawel szymczyk
 */

class RestERPClient implements ERPClient {

    private final RestTemplate restTemplate;
    private final String url;

    public RestERPClient(String url) {
        this.url = url;
        this.restTemplate = new RestTemplate();
    }

    @Override
    public void send(PassengerId passengerId, Money fee) {
        restTemplate.postForEntity(url, "{\n"
                + "  \"userId\": \""+ passengerId.asString() + "\",\n"
                + "  \"amount\": \"" + fee.asString() + "\",\n"
                + "  \"operation\": \"CHARGE_V_201_1\"\n"
                + "}", Void.class);
    }
}
