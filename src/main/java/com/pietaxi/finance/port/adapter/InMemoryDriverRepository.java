package com.pietaxi.finance.port.adapter;

import java.util.HashMap;
import java.util.Map;

import com.pietaxi.finance.domain.model.Driver;
import com.pietaxi.finance.domain.model.DriverRepository;
import com.pietaxi.sharedkernel.DriverId;

public class InMemoryDriverRepository implements DriverRepository {

    private final Map<String, Driver> storage = new HashMap<>();

    @Override
    public void save(Driver driver) {
        storage.put(driver.getLicenceId(), driver);
    }

    @Override
    public Driver findByDriverId(DriverId driverId) {
        return storage.get(driverId);
    }

}
