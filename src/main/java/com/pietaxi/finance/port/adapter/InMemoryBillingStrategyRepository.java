package com.pietaxi.finance.port.adapter;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.pietaxi.finance.domain.model.BillingStrategy;
import com.pietaxi.finance.domain.model.BillingStrategyRepository;
import com.pietaxi.finance.domain.model.Driver;

public class InMemoryBillingStrategyRepository implements BillingStrategyRepository {

    private final Map<Driver, BillingStrategy> storage = new HashMap<>();

    @Override
    public Optional<BillingStrategy> findStrategyForDriver(Driver driver) {
        return Optional.ofNullable(storage.get(driver));
    }
}
