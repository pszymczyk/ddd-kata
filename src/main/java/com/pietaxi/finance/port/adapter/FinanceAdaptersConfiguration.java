package com.pietaxi.finance.port.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pietaxi.finance.application.ChargePassengerFeature;
import com.pietaxi.finance.domain.model.BillFactory;
import com.pietaxi.finance.domain.model.BillRepository;
import com.pietaxi.finance.domain.model.BillingStrategyRepository;
import com.pietaxi.finance.domain.model.DriverRepository;
import com.pietaxi.finance.domain.model.ERPClient;
import com.pietaxi.rides.RidesFacade;

@Configuration
public class FinanceAdaptersConfiguration {

    @Autowired
    RidesFacade ridesFacade;

    @Value("${erp.url:http://localhost:8081}")
    String erpUrl;

    @Bean
    public FinanceDomainEventsListener financeDomainEventsListener() {
        BillFactory billFactory = new BillFactory(ridesFacade, financeDriverRepository(), billingStrategyRepository());
        ChargePassengerFeature chargePassengerFeature = new ChargePassengerFeature(billRepository(), billFactory, erpClient());

        return new FinanceDomainEventsListener(chargePassengerFeature);
    }

    @Bean
    public ERPClient erpClient() {
        return new RestERPClient(erpUrl);
    }

    @Bean
    public DriverRepository financeDriverRepository() {
        return new InMemoryDriverRepository();
    }

    @Bean
    BillingStrategyRepository billingStrategyRepository() {
        return new InMemoryBillingStrategyRepository();
    }

    @Bean
    BillRepository billRepository() {
        return new InMemoryBillRepository();
    }

}
