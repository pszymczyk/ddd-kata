package com.pietaxi.finance.application;

import com.pietaxi.finance.domain.model.Bill;
import com.pietaxi.finance.domain.model.BillFactory;
import com.pietaxi.finance.domain.model.BillRepository;
import com.pietaxi.finance.domain.model.ERPClient;
import com.pietaxi.sharedkernel.RideId;

/**
 * @author pawel szymczyk
 */
public class ChargePassengerFeature {

    private final BillRepository billRepository;
    private final BillFactory billFactory;
    private final ERPClient erpClient;

    public ChargePassengerFeature(BillRepository billRepository, BillFactory billFactory, ERPClient erpClient) {
        this.billRepository = billRepository;
        this.billFactory = billFactory;
        this.erpClient = erpClient;
    }

    public void chargePassenger(RideId rideId) {
        Bill bill = billFactory.create(rideId);
        billRepository.save(bill);
        erpClient.send(bill.getPassengerId(), bill.account());
    }
}
