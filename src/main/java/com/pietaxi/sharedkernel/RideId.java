package com.pietaxi.sharedkernel;

import java.util.Objects;
import java.util.UUID;

public final class RideId {

    private final UUID id;

    public static RideId random() {
        return new RideId(UUID.randomUUID());
    }

    public static RideId from(String id) {
        return new RideId(UUID.fromString(id));
    }

    private RideId(UUID id) {
        this.id = id;
    }

    public String asString() {
        return id.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RideId rideId = (RideId) o;
        return id.equals(rideId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
