package com.pietaxi.sharedkernel;

import java.util.Objects;
import java.util.UUID;

public final class DriverId {

    private final UUID id;

    public static DriverId random() {
        return from(UUID.randomUUID());
    }

    public static DriverId from(UUID id) {
        return new DriverId(id);
    }

    private DriverId(UUID id) {
        this.id = id;
    }

    public static DriverId from(String driverId) {
        return new DriverId(UUID.fromString(driverId));
    }

    public String asString() {
        return id.toString();
    }

    @Override
    public String toString() {
        return "DriverId{" +
                "id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DriverId driverId = (DriverId) o;
        return id.equals(driverId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
