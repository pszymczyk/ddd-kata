package com.pietaxi.sharedkernel;

import java.util.Objects;
import java.util.UUID;

/**
 * @author pawel szymczyk
 */
public final class PassengerId {

    private final UUID id;

    public static PassengerId random() {
        return from(UUID.randomUUID());
    }

    public static PassengerId from(UUID id) {
        return new PassengerId(id);
    }

    private PassengerId(UUID id) {
        this.id = id;
    }

    public static PassengerId from(String passengerId) {
        return new PassengerId(UUID.fromString(passengerId));
    }

    public String asString() {
        return id.toString();
    }

    @Override
    public String toString() {
        return "PassengerId{" +
                "id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PassengerId that = (PassengerId) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
